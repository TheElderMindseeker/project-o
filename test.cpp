#include <iostream>
#include "lexical/lexer.h"
#include "syntactical/parser.h"
#include "syntactical/TreeNode.h"
#include "semantical/generics/GenericMagician.h"
#include "semantical/helpers/ClassDecalrationHelper.h"
#include "semantical/SemanticalAnalyzer.h"
#include "semantical/helpers/TablesPrinter.h"
#include "semantical/visitors/ClassStructureVisitor.h"
#include "semantical/visitors/TypeDeducingVisitor.h"
#include "semantical/tables/VariablesTable.h"
#include "semantical/tables/MethodsTable.h"
#include "semantical/tables/ConstructorsTable.h"
#include "semantical/tables/VTable.h"
#include "semantical/codegen/LayoutGenerator.h"
#include "semantical/tables/ClassLayout.h"
#include "semantical/codegen/CodeGenVisitor.h"
#include "semantical/helpers/CodeOptimizer.h"

void processFile(const std::string &fileName, const std::string &fileToWrite) {
    std::ifstream ifs(fileName);
    std::string content((std::istreambuf_iterator<char>(ifs)),
                        (std::istreambuf_iterator<char>()));
    std::ofstream ofs;
    ofs.open(fileToWrite);

    Lexer lexer(content);
    Parser parser(lexer.get_tokens());
    parser.parse();

    if (parser.getError() != "") {
        std::cerr << "\n\nError: " << parser.getError()
        << " column: " << parser.getTree()->getColumnInSource()
        << " row: " << parser.getTree()->getRowInSource() << std::endl;
        return;
    }

    TreeNode *tree = parser.getTree();

    // std::cout << tree->toJSON();

    auto gm = new GenericMagician(tree);

    if (gm->errorStatus()) {
        std::cerr << "\n\nError!\n" << gm->getErrorDescription() << std::endl;
        return;
    }

    TreeNode *newTree = gm->getNewTree();

    auto semAnalyzer = new SemanticalAnalyzer(gm);
    semAnalyzer->parse(newTree);

    if (semAnalyzer->isHasError()) {
        std::cerr<<semAnalyzer->getErrorDescription();
        return;
    }

    /*
    std::cout << "\n\n------- Class Table -------\n\n";
    std::cout << TablesPrinter::classTableToStr(semAnalyzer->getClassTable());
     */

    auto csVisitor = new ClassStructureVisitor(
        semAnalyzer->getClassTable(),
        gm
    );
    newTree->accept(csVisitor);

    if (csVisitor->errorStatus()) {
        std::cerr << "\n\n+++++++ Error Occured: +++++++\n\n"
                  << csVisitor->getErrorDescription() << std::endl;

        delete semAnalyzer;
        delete csVisitor;
        return;
    }

    /*
    std::cout << "\n\n======= Class Structures =======\n\n";

    for (auto &entry : csVisitor->getStructures()) {
        std::cout << "Class ID: " << entry.first << "\n\nVarTable:\n\n";
        auto vt = entry.second->variablesTable;
        std::cout << *vt << std::endl << "MethodTable:\n\n";
        auto mt = entry.second->methodsTable;
        std::cout << *mt << std::endl << "CtorsTable:\n\n";
        auto ct = entry.second->ctorsTable;
        std::cout << *ct << std::endl;
    }
     */

    auto vTable = new VTable(semAnalyzer->getClassTable(),
                             csVisitor->getStructures());

    //std::cout << "\n\n******* VTables *******\n\n";
    ofs << TablesPrinter::VTableToStr(vTable,
                                            semAnalyzer->getClassTable());
    //std::cout << "\n\n" << std::endl;

    auto tdVisitor = new TypeDeducingVisitor(semAnalyzer->getClassTable(),
                                             csVisitor->getStructures());
    newTree->accept(tdVisitor);

    if (tdVisitor->errorStatus()) {
        std::cerr<<tdVisitor->getErrorDescription();
        return;
    }

    auto lg = new LayoutGenerator(semAnalyzer->getClassTable(),
                                  csVisitor, tdVisitor);

    //std::cout << "\n\n####### Class Layouts: #######\n\n";
    for (int i =6; i< lg->getLayouts().size(); i++) {
        auto entry = lg->getLayouts().at(i);
        ofs << entry->getLayout();
    }

    auto codeGen = new CodeGenVisitor(semAnalyzer->getClassTable(), tdVisitor);
    newTree->accept(codeGen);
    if (codeGen->errorStatus()) {
        std::cerr << codeGen->getErrorDescription();
    } else {
        ofs << CodeOptimizer::optimize(codeGen->getResult());
    }

    delete tdVisitor;
    delete vTable;
    delete semAnalyzer;
    delete csVisitor;

    ofs.close();
}

int main(int argc, char **argv) {
    /*
    if (argc != 2) {
        std::cerr << "Usage: test_cpo <path-to-test-suite>" << std::endl;
        return -1;
    }

    std::string file1 = std::string(argv[1]) + "/normal.cpo";
    std::string file2 = std::string(argv[1]) + "/error.cpo";

    std::cout << "\n\n\n*********& normal.cpo &*********\n\n\n";
    processFile(file1);
    std::cout << "\n\n\n*********& error.cpo &*********\n\n\n";
    processFile(file2);
     */

    processFile("inherit.cpo", "niyaz.povm");

    return 0;
}
