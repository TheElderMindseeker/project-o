#include <utility>

//
// Created by Andrey on 01.12.2018.
//

#include <vector>
#include "../syntactical/parser.h"
#include "../syntactical/TreeNode.h"

Parser::Parser(std::vector<Token> vector) : tokens(std::move(vector)),
                                            posInTokensVector(0),
                                            status(0),
                                            errorRow(0),
                                            errorColumn(0) {}

void Parser::parse() {
    treeRoot = parseProgram();
}

TreeNode* Parser::getTree() {
    return treeRoot;
}

TreeNode* Parser::parseProgram() {

//    Program ::= { ClassDeclaration }

    TreeNode* node = new TreeNode(PE_PROGRAM, 1, 1);
    TreeNode* classDeclaration;

    while (getCurrentToken().type != TK_ENDMARKER) {
        classDeclaration = parseClassDeclaration();
        if (status != 0) {
            delete node;
            delete classDeclaration;
            node = new TreeNode(PE_ERROR, errorRow, errorColumn);
            break;
        }
        if (classDeclaration->getNodeType() == PE_CLASS_DECLARATION) {
            node->appendChild(classDeclaration);
        }

    }
    return node;
}

TreeNode* Parser::parseClassDeclaration() {

//    ClassDeclaration ::= 'class' ClassName [ extends NAME ] 'is'
//    { MemberDeclaration }
//    'end'

    if (getCurrentToken().type != TK_CLASS) {
        // This is not the construction we are searching for
        return new TreeNode(PE_EMPTY, getCurrentToken().row, getCurrentToken().column);
    }
    TreeNode* node = new TreeNode(PE_CLASS_DECLARATION, getCurrentToken().row, getCurrentToken().column);

    moveToNextToken();

    TreeNode* className = parseClassName();

    if (className->getNodeType() != PE_CLASS_NAME) {
        setError("Class name expected", className->getRowInSource(), className->getColumnInSource());
        node->deleteChildren();
        delete node;
        return className;
    }

    node->appendChild(className);

    if (getCurrentToken().type == TK_EXTENDS) {
        moveToNextToken();
        TreeNode* extendsName = parseName();

        if (extendsName->getNodeType() != PE_NAME) {
            setError("Name expected", extendsName->getRowInSource(), extendsName->getColumnInSource());
            node->deleteChildren();
            delete node;
            return extendsName;
        }

        // Need to think how to highlight that it is inheritance...
        node->appendChild(extendsName);
    }

    if (getCurrentToken().type != TK_IS) {
        setError("Missed 'is' keyword", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }

    moveToNextToken();

    TreeNode* memberDeclaration = parseMemberDeclaration();
    while (memberDeclaration->getNodeType() == PE_MEMBER_DECLARATION) {
        node->appendChild(memberDeclaration);
        memberDeclaration = parseMemberDeclaration();
    }

    if (getCurrentToken().type != TK_END) {
        setError("Missed 'end' keyword", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }

    moveToNextToken();
    return node;
}

TreeNode* Parser::parseClassName() {

//    ClassName ::= NAME [ '[' NAME ']' ]

    TreeNode* name = parseName();
    if (name->getNodeType()!= PE_NAME) {
        // This is not the construction we are searching for
        return name;
    }

    TreeNode* node = new TreeNode(PE_CLASS_NAME, name->getRowInSource(), name->getColumnInSource());
    node->appendChild(name);

    if (getCurrentToken().type == TK_LEFT_BRACK) {
        moveToNextToken();

        TreeNode* name = parseName();
        if (name->getNodeType() != PE_NAME) {
            setError("Name expected", name->getRowInSource(), name->getColumnInSource());
            node->deleteChildren();
            delete node;
            return name;
        }

        node->appendChild(name);

        if (getCurrentToken().type != TK_RIGHT_BRACK) {
            setError("Missed closing bracket", getCurrentToken().row, getCurrentToken().column);
            node->deleteChildren();
            delete node;
            return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
        }
        moveToNextToken();
    }

    return node;
}

TreeNode* Parser::parseMemberDeclaration() {

//    MemberDeclaration ::=
//            VariableDeclaration
//            | MethodDeclaration
//            | ConstructorDeclaration

    TreeNode* tempNode = parseVariableDeclaration();
    if (tempNode->getNodeType() != PE_VARIABLE_DECLARATION) {
        delete tempNode;
        tempNode = parseMethodDeclaration();
    }
    if (tempNode->getNodeType() != PE_VARIABLE_DECLARATION &&
            tempNode->getNodeType() != PE_METHOD_DECLARATION) {
        delete tempNode;
        tempNode = parseConstructorDeclaration();
    }
    if (tempNode->getNodeType() != PE_VARIABLE_DECLARATION &&
            tempNode->getNodeType() != PE_METHOD_DECLARATION &&
            tempNode->getNodeType() != PE_CONSTRUCTOR_DECLARATION) {
        // This is not the construction we are searching for
        return tempNode;
    }

    TreeNode* node = new TreeNode(PE_MEMBER_DECLARATION, tempNode->getRowInSource(), tempNode->getColumnInSource());
    node->appendChild(tempNode);
    return node;
}

TreeNode* Parser::parseVariableDeclaration() {

//    VariableDeclaration ::= 'var' NAME ':' Expression

    if (getCurrentToken().type != TK_VAR) {
        // This is not the construction we are searching for
        return new TreeNode(PE_EMPTY, getCurrentToken().row, getCurrentToken().column);
    }

    TreeNode* node = new TreeNode(PE_VARIABLE_DECLARATION, getCurrentToken().row, getCurrentToken().column);

    moveToNextToken();

    TreeNode* name = parseName();
    if (name->getNodeType() != PE_NAME) {
        setError("Missed variable name", name->getRowInSource(), name->getColumnInSource());
        node->deleteChildren();
        delete node;
        return name;
    }

    node->appendChild(name);

    if (getCurrentToken().type != TK_COLON) {
        setError("Missed colon in variable declaration", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }
    moveToNextToken();

    TreeNode* expression = parseExpression();
    if (expression->getNodeType() != PE_EXPRESSION) {
        setError("Missed expression", expression->getRowInSource(), expression->getColumnInSource());
        node->deleteChildren();
        delete node;
        return expression;
    }

    node->appendChild(expression);

    return node;
}

TreeNode* Parser::parseMethodDeclaration() {

//    MethodDeclaration ::= 'method' NAME [ Parameters ] [ ':' ClassName ]
//    'is' Body 'end'

    if (getCurrentToken().type != TK_METHOD) {
        // This is not the construction we are searching for
        return new TreeNode(PE_EMPTY, getCurrentToken().row, getCurrentToken().column);
    }

    TreeNode* node = new TreeNode(PE_METHOD_DECLARATION, getCurrentToken().row, getCurrentToken().column);

    moveToNextToken();

    TreeNode* name = parseName();
    if (name->getNodeType() != PE_NAME) {
        setError("Missed method name", name->getRowInSource(), name->getColumnInSource());
        node->deleteChildren();
        delete node;
        return name;
    }

    node->appendChild(name);

    TreeNode* parameters = parseParameters();
    if (parameters->getNodeType() == PE_PARAMETERS) {
        node->appendChild(parameters);
    }

    if (getCurrentToken().type == TK_COLON) {
        moveToNextToken();

        name = parseClassName();
        if (name->getNodeType() != PE_CLASS_NAME) {
            setError("Missed class name", name->getRowInSource(), name->getColumnInSource());
            node->deleteChildren();
            delete node;
            return name;
        }
        node->appendChild(name);
    }

    if (getCurrentToken().type != TK_IS) {
        setError("Missed 'is' keyword", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }

    moveToNextToken();

    TreeNode* body = parseBody();
    if (body->getNodeType() != PE_BODY) {
        setError("Missed method body", body->getRowInSource(), body->getColumnInSource());
        node->deleteChildren();
        delete node;
        return body;
    }

    node->appendChild(body);

    if (getCurrentToken().type != TK_END) {
        setError("Missed 'end' keyword", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }

    moveToNextToken();
    return node;
}

TreeNode* Parser::parseParameters() {

//    Parameters ::= '(' ParameterDeclaration { ',' ParameterDeclaration } ')'

    if (getCurrentToken().type != TK_LEFT_PAREN) {
        // This is not the construction we are searching for
        return new TreeNode(PE_EMPTY, getCurrentToken().row, getCurrentToken().column);
    }

    TreeNode* node = new TreeNode(PE_PARAMETERS, getCurrentToken().row, getCurrentToken().column);

    moveToNextToken();

    TreeNode* parameterDeclaration = parseParameterDeclaration();
    if (parameterDeclaration->getNodeType() != PE_PARAMETER_DECLARATION) {
        setError("Missed parameters",
                parameterDeclaration->getRowInSource(),
                parameterDeclaration->getColumnInSource());
        node->deleteChildren();
        delete node;
        return parameterDeclaration;
    }

    node->appendChild(parameterDeclaration);

    while (getCurrentToken().type == TK_COMMA) {
        moveToNextToken();
        parameterDeclaration = parseParameterDeclaration();
        if (parameterDeclaration->getNodeType() != PE_PARAMETER_DECLARATION) {
            setError("Parameter definition expected after the comma",
                    parameterDeclaration->getRowInSource(),
                    parameterDeclaration->getColumnInSource());
            node->deleteChildren();
            delete node;
            return parameterDeclaration;
        }

        node->appendChild(parameterDeclaration);
    }

    if (getCurrentToken().type != TK_RIGHT_PAREN) {
        setError("Missed closing parenthesis", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }
    moveToNextToken();

    return node;
}

TreeNode* Parser::parseParameterDeclaration() {

//    ParameterDeclaration ::= NAME ':' ClassName

    TreeNode* name = parseName();
    if (name->getNodeType() != PE_NAME) {
        // This is not the construction we are searching for
        return name;
    }

    TreeNode* node = new TreeNode(PE_PARAMETER_DECLARATION, name->getRowInSource(), name->getColumnInSource());

    node->appendChild(name);

    if (getCurrentToken().type != TK_COLON) {
        setError("Missed colon", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }

    moveToNextToken();

    TreeNode* className = parseClassName();
    if (className->getNodeType() != PE_CLASS_NAME) {
        setError("Missed type of parameter", className->getRowInSource(), className->getColumnInSource());
        node->deleteChildren();
        delete node;
        return className;
    }

    node->appendChild(className);

    return node;

}

TreeNode* Parser::parseBody() {

//    Body ::= { VariableDeclaration | Statement }

    TreeNode* node = new TreeNode(PE_BODY, getCurrentToken().row, getCurrentToken().column);

    TreeNode* tempNode = parseVariableDeclaration();
    if (tempNode->getNodeType() != PE_VARIABLE_DECLARATION) {
        delete tempNode;
        tempNode = parseStatement();
    }

    while (tempNode->getNodeType() == PE_VARIABLE_DECLARATION ||
            tempNode->getNodeType() == PE_STATEMENT) {
        node->appendChild(tempNode);

        tempNode = parseVariableDeclaration();
        if (tempNode->getNodeType() != PE_VARIABLE_DECLARATION) {
            delete tempNode;
            tempNode = parseStatement();
        }
    }

    return node;
}

TreeNode* Parser::parseConstructorDeclaration() {

//    ConstructorDeclaration ::= 'this' [ Parameters ] 'is' Body 'end'

    if (getCurrentToken().type != TK_THIS) {
        // This is not the construction we are searching for
        return new TreeNode(PE_EMPTY, getCurrentToken().row, getCurrentToken().column);
    }

    TreeNode* node = new TreeNode(PE_CONSTRUCTOR_DECLARATION, getCurrentToken().row, getCurrentToken().column);

    moveToNextToken();

    TreeNode* tempNode = parseParameters();
    if (tempNode->getNodeType() == PE_PARAMETERS) {
        node->appendChild(tempNode);
    }

    if (getCurrentToken().type != TK_IS) {
        setError("Missed 'is' token", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }

    moveToNextToken();

    tempNode = parseBody();

    if (tempNode->getNodeType() != PE_BODY) {
        setError("Missed constructor body", tempNode->getRowInSource(), tempNode->getColumnInSource());
        node->deleteChildren();
        delete node;
        return tempNode;
    }

    node->appendChild(tempNode);

    if (getCurrentToken().type != TK_END) {
        setError("Missed 'end' keyword", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }

    moveToNextToken();

    return node;
}

TreeNode* Parser::parseStatement() {

//    Statement ::=
//            Assignment
//            | WhileLoop
//            | IfStatement
//            | ReturnStatement

    TreeNode* node = new TreeNode(PE_STATEMENT, getCurrentToken().row, getCurrentToken().column);
    TreeNode* tempNode = parseAssignment();
    if (tempNode->getNodeType() == PE_ASSIGNMENT) {
        node->appendChild(tempNode);
        return node;
    }
    delete tempNode;
    tempNode = parseWhileLoop();
    if (tempNode->getNodeType() == PE_WHILE_LOOP) {
        node->appendChild(tempNode);
        return node;
    }
    delete tempNode;
    tempNode = parseIfStatement();
    if (tempNode->getNodeType() == PE_IF_STATEMENT) {
        node->appendChild(tempNode);
        return node;
    }
    delete tempNode;
    tempNode = parseReturnStatement();
    if (tempNode->getNodeType() == PE_RETURN_STATEMENT) {
        node->appendChild(tempNode);
        return node;
    }

    // This is not the construction we are searching for
    delete node;
    return tempNode;
}

TreeNode* Parser::parseAssignment() {

//    Assignment ::= NAME ':=' Expression

    TreeNode* name = parseName();
    if (name->getNodeType() != PE_NAME) {
        // This is not the construction we are searching for
        return name;
    }

    TreeNode* node = new TreeNode(PE_ASSIGNMENT, name->getRowInSource(), name->getColumnInSource());
    node->appendChild(name);

    if (getCurrentToken().type != TK_ASSIGNMENT) {
        setError("Missed assignment symbol", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }

    moveToNextToken();

    TreeNode* expression = parseExpression();

    if (expression->getNodeType() != PE_EXPRESSION) {
        setError("Missed expression in the assignment", expression->getRowInSource(), expression->getColumnInSource());
        node->deleteChildren();
        delete node;
        return expression;
    }

    node->appendChild(expression);

    return node;
}

TreeNode* Parser::parseWhileLoop() {

//    WhileLoop ::= 'while' Expression 'loop' Body 'end'

    if (getCurrentToken().type != TK_WHILE) {
        // This is not the construction we are searching for
        return new TreeNode(PE_EMPTY, getCurrentToken().row, getCurrentToken().column);
    }

    TreeNode* node = new TreeNode(PE_WHILE_LOOP, getCurrentToken().row, getCurrentToken().column);

    moveToNextToken();

    TreeNode* expression = parseExpression();

    if (expression->getNodeType() != PE_EXPRESSION) {
        setError("Missed condition", expression->getRowInSource(), expression->getColumnInSource());
        node->deleteChildren();
        delete node;
        return expression;
    }
    if (getCurrentToken().type != TK_LOOP) {
        setError("Missed 'loop' keyword", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }

    node->appendChild(expression);
    moveToNextToken();
    TreeNode* body = parseBody();

    if (body->getNodeType() != PE_BODY) {
        setError("Missed body of the loop", body->getRowInSource(), body->getColumnInSource());
        node->deleteChildren();
        delete node;
        return body;
    }

    node->appendChild(body);

    if (getCurrentToken().type != TK_END) {
        setError("Missed 'end' keyword", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }
    moveToNextToken();

    return node;
}

TreeNode* Parser::parseIfStatement() {

//    IfStatement ::= 'if' Expression 'then' Body [ 'else' Body ] 'end'

    if (getCurrentToken().type != TK_IF) {
        // This is not the construction we are searching for
        return new TreeNode(PE_EMPTY, getCurrentToken().row, getCurrentToken().column);
    }

    TreeNode* node = new TreeNode(PE_IF_STATEMENT, getCurrentToken().row, getCurrentToken().column);

    moveToNextToken();

    TreeNode* expression = parseExpression();

    if (expression->getNodeType() != PE_EXPRESSION) {
        setError("Missed condition", expression->getRowInSource(), expression->getColumnInSource());
        node->deleteChildren();
        delete node;
        return expression;
    }

    node->appendChild(expression);

    if (getCurrentToken().type != TK_THEN) {
        setError("Missed 'then' keyword", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }
    moveToNextToken();


    TreeNode* body = parseBody();

    if (body->getNodeType() != PE_BODY) {
        setError("Missed body of then-part of conditional statement",
                body->getRowInSource(),
                body->getColumnInSource());
        node->deleteChildren();
        delete node;
        return body;
    }

    node->appendChild(body);

    if (getCurrentToken().type == TK_ELSE) {
        // TODO Think how to make difference between then and else bodies
        moveToNextToken();
        body = parseBody();
        if (body->getNodeType() != PE_BODY) {
            setError("Missed body of else-part of conditional statement",
                    body->getRowInSource(),
                    body->getColumnInSource());
            node->deleteChildren();
            delete node;
            return body;
        }

        node->appendChild(body);
    }

    if (getCurrentToken().type != TK_END) {
        setError("Missed 'end' keyword", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }

    moveToNextToken();

    return node;
}

TreeNode* Parser::parseReturnStatement() {

//    ReturnStatement ::= 'return' [ Expression ]

    if (getCurrentToken().type != TK_RETURN) {
        // This is not the construction we are searching for
        return new TreeNode(PE_EMPTY, getCurrentToken().row, getCurrentToken().column);
    }

    TreeNode* node = new TreeNode(PE_RETURN_STATEMENT, getCurrentToken().row, getCurrentToken().column);

    moveToNextToken();

    TreeNode* expression = parseExpression();

    if (expression->getNodeType() == PE_EXPRESSION) {
        node->appendChild(expression);
    }

    return node;
}

TreeNode* Parser::parseExpression() {

//    Expression ::= Primary { '.' NAME [ Arguments ] }

    TreeNode* primary = parsePrimary();
    if (primary->getNodeType() != PE_PRIMARY) {
        // This is not the construction we are searching for
        return primary;
    }

    TreeNode* node = new TreeNode(PE_EXPRESSION, primary->getRowInSource(), primary->getColumnInSource());
    node->appendChild(primary);

    TreeNode *name, *arguments;
    while (getCurrentToken().type == TK_ACCESS) {
        moveToNextToken();
        name = parseName();
        if (name->getNodeType() != PE_NAME) {
            setError("Missed name of parameter or method", name->getRowInSource(), name->getColumnInSource());
            node->deleteChildren();
            delete node;
            return name;
        }

        node->appendChild(name);

        arguments = parseArguments();
        if (arguments->getNodeType() == PE_ARGUMENTS) {
            node->appendChild(arguments);
        }
    }

    return node;
}

TreeNode* Parser::parseArguments() {

//    Arguments ::= '(' Expression { ',' Expression } ')'
    if (getCurrentToken().type != TK_LEFT_PAREN) {
        // This is not the construction we are searching for
        return new TreeNode(PE_EMPTY, getCurrentToken().row, getCurrentToken().column);
    }

    TreeNode* node = new TreeNode(PE_ARGUMENTS, getCurrentToken().row, getCurrentToken().column);

    moveToNextToken();

    TreeNode* expression = parseExpression();

    if (expression->getNodeType() != PE_EXPRESSION) {
        setError("Missed expressions in parentheses", expression->getRowInSource(), expression->getColumnInSource());
        node->deleteChildren();
        delete node;
        return expression;
    }

    node->appendChild(expression);

    while (getCurrentToken().type == TK_COMMA) {
        moveToNextToken();

        expression = parseExpression();

        if (expression->getNodeType() != PE_EXPRESSION) {
            setError("Missed expression after the comma",
                    expression->getRowInSource(),
                    expression->getColumnInSource());
            node->deleteChildren();
            delete node;
            return expression;
        }

        node->appendChild(expression);
    }

    if (getCurrentToken().type != TK_RIGHT_PAREN) {
        setError("Missed closing parenthesis", getCurrentToken().row, getCurrentToken().column);
        node->deleteChildren();
        delete node;
        return new TreeNode(PE_ERROR, getCurrentToken().row, getCurrentToken().column);
    }

    moveToNextToken();

    return node;
}

TreeNode* Parser::parsePrimary() {

//    Primary ::=
//            INTEGER
//            | REAL
//            | BOOLEAN
//            | 'this'
//            | ClassName [ Arguments ]

    TreeNode* node = new TreeNode(PE_PRIMARY, getCurrentToken().row, getCurrentToken().column);
    if (getCurrentToken().type == TK_INTEGER ||
        getCurrentToken().type == TK_REAL ||
        getCurrentToken().type == TK_BOOLEAN ||
        getCurrentToken().type == TK_THIS) {
        node->setToken(getCurrentToken());
        moveToNextToken();
    } else {
        TreeNode* className = parseClassName();
        if (className->getNodeType() != PE_CLASS_NAME) {
            // This is not the construction we are searching for
            delete node;
            return className;
        }
        node->appendChild(className);

        TreeNode* arguments = parseArguments();
        if (arguments->getNodeType() == PE_ARGUMENTS) {
            node->appendChild(arguments);
        }
    }
    return node;
}

TreeNode* Parser::parseName() {
    if (getCurrentToken().type != TK_NAME) {
        // This is not the construction we are searching for
        return new TreeNode(PE_EMPTY, getCurrentToken().row, getCurrentToken().column);
    }
    TreeNode* node = new TreeNode(PE_NAME, getCurrentToken().row, getCurrentToken().column);
    node->setToken(getCurrentToken());
    moveToNextToken();
    return node;
}

const std::string &Parser::getError() const {
    return error;
}

void Parser::setError(const std::string &error, unsigned int errorRow, unsigned int errorColumn) {
    if (status != -1) {
        Parser::error = error;
        Parser::errorRow = errorRow;
        Parser::errorColumn = errorColumn;
        Parser::status = -1;
    }
}

Token &Parser::getCurrentToken() {
    if (posInTokensVector < tokens.size()) {
        return tokens.at(posInTokensVector);
    } else {
        Token* token = new Token;
        token->type = TK_ENDMARKER;
        return *token;
    }
}

void Parser::moveToNextToken() {
    posInTokensVector++;
}

unsigned int Parser::getErrorRow() const {
    return errorRow;
}

unsigned int Parser::getErrorColumn() const {
    return errorColumn;
}

