//
// Created by Andrey on 02.12.2018.
//

#include <vector>
#include "TreeNode.h"

TreeNode::TreeNode(NodeType nodeType)
        : nodeType(nodeType), rowInSource(0), columnInSource(0) {}

TreeNode::TreeNode(NodeType nodeType, unsigned int rowInSource,
                   unsigned int columnInSource)
        : nodeType(nodeType), rowInSource(rowInSource),
          columnInSource(columnInSource) {}

NodeType TreeNode::getNodeType() const {
    return nodeType;
}

const std::vector<TreeNode*> &TreeNode::getChildren() const {
    return children;
}

void TreeNode::appendChild(TreeNode *child) {
    children.push_back(child);
}

const Token &TreeNode::getToken() const {
    return token;
}

unsigned int TreeNode::getRowInSource() const {
    return rowInSource;
}

unsigned int TreeNode::getColumnInSource() const {
    return columnInSource;
}


void TreeNode::setToken(const Token &token) {
    TreeNode::token = token;
}

void TreeNode::deleteChildren() {
    for (auto &child : children) {
        child->deleteChildren();
        delete child;
    }
}

std::string TreeNode::toJSON() {
    std::string result = R"({ "type": ")" + getTypeString() + R"(", "children": [)";
    if (!children.empty()) {
        for (int i = 0; i < children.size() - 1; ++i) {
            result += children.at(i)->toJSON() + ", ";
        }
        result += children.at(children.size() - 1)->toJSON();
    }
    result += R"(], "str_value": ")" + token.str_value + R"("})";
    return result;
}

std::string TreeNode::getTypeString() const {
    switch (nodeType) {
        case PE_PROGRAM:
            return "PE_PROGRAM";
        case PE_CLASS_DECLARATION:
            return "PE_CLASS_DECLARATION";
        case PE_CLASS_NAME:
            return "PE_CLASS_NAME";
        case PE_MEMBER_DECLARATION:
            return "PE_MEMBER_DECLARATION";
        case PE_VARIABLE_DECLARATION:
            return "PE_VARIABLE_DECLARATION";
        case PE_METHOD_DECLARATION:
            return "PE_METHOD_DECLARATION";
        case PE_PARAMETERS:
            return "PE_PARAMETERS";
        case PE_PARAMETER_DECLARATION:
            return "PE_PARAMETER_DECLARATION";
        case PE_BODY:
            return "PE_BODY";
        case PE_CONSTRUCTOR_DECLARATION:
            return "PE_CONSTRUCTOR_DECLARATION";
        case PE_STATEMENT:
            return "PE_STATEMENT";
        case PE_ASSIGNMENT:
            return "PE_ASSIGNMENT";
        case PE_WHILE_LOOP:
            return "PE_WHILE_LOOP";
        case PE_IF_STATEMENT:
            return "PE_IF_STATEMENT";
        case PE_RETURN_STATEMENT:
            return "PE_RETURN_STATEMENT";
        case PE_EXPRESSION:
            return "PE_EXPRESSION";
        case PE_ARGUMENTS:
            return "PE_ARGUMENTS";
        case PE_PRIMARY:
            return "PE_PRIMARY";
        case PE_NAME:
            return "PE_NAME";
        case PE_EMPTY:
            return "PE_EMPTY";
        case PE_ERROR:
            return "PE_ERROR";
    }
}

void TreeNode::accept(BaseVisitor *visitor) {
    switch (nodeType) {
        case PE_PROGRAM:
            visitor->visitProgram(this);
            break;
        case PE_CLASS_DECLARATION:
            visitor->visitClassDeclaration(this);
            break;
        case PE_CLASS_NAME:
            visitor->visitClassName(this);
            break;
        case PE_MEMBER_DECLARATION:
            visitor->visitMemberDeclaration(this);
            break;
        case PE_VARIABLE_DECLARATION:
            visitor->visitVariableDeclaration(this);
            break;
        case PE_METHOD_DECLARATION:
            visitor->visitMethodDeclaration(this);
            break;
        case PE_PARAMETERS:
            visitor->visitParameters(this);
            break;
        case PE_PARAMETER_DECLARATION:
            visitor->visitParameterDeclaration(this);
            break;
        case PE_BODY:
            visitor->visitBody(this);
            break;
        case PE_CONSTRUCTOR_DECLARATION:
            visitor->visitConstructorDeclaration(this);
            break;
        case PE_STATEMENT:
            visitor->visitStatement(this);
            break;
        case PE_ASSIGNMENT:
            visitor->visitAssignment(this);
            break;
        case PE_WHILE_LOOP:
            visitor->visitWhileLoop(this);
            break;
        case PE_IF_STATEMENT:
            visitor->visitIfStatement(this);
            break;
        case PE_RETURN_STATEMENT:
            visitor->visitReturnStatement(this);
            break;
        case PE_EXPRESSION:
            visitor->visitExpression(this);
            break;
        case PE_ARGUMENTS:
            visitor->visitArguments(this);
            break;
        case PE_PRIMARY:
            visitor->visitPrimary(this);
            break;
        case PE_NAME:
            visitor->visitName(this);
            break;
        case PE_EMPTY:

            break;
        case PE_ERROR:

            break;
    }
}
