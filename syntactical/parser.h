//
// Created by Andrey on 01.12.2018.
//

#ifndef PROJECT_PARSER_H
#define PROJECT_PARSER_H

#include <string>
#include <vector>
#include "../lexical/lexer.h"

class TreeNode;

enum NodeType {
    PE_PROGRAM,
    PE_CLASS_DECLARATION,
    PE_CLASS_NAME,
    PE_MEMBER_DECLARATION,
    PE_VARIABLE_DECLARATION,
    PE_METHOD_DECLARATION,
    PE_PARAMETERS,
    PE_PARAMETER_DECLARATION,
    PE_BODY,
    PE_CONSTRUCTOR_DECLARATION,
    PE_STATEMENT,
    PE_ASSIGNMENT,
    PE_WHILE_LOOP,
    PE_IF_STATEMENT,
    PE_RETURN_STATEMENT,
    PE_EXPRESSION,
    PE_ARGUMENTS,
    PE_PRIMARY,
    PE_NAME,
    PE_EMPTY,
    PE_ERROR
};

class Parser {
public:
    Parser(std::vector<Token>);
    void parse();
    TreeNode* getTree();
    const std::string &getError() const;
    unsigned int getErrorRow() const;
    unsigned int getErrorColumn() const;

private:
    TreeNode* treeRoot;
    std::vector<Token> tokens;
    unsigned int posInTokensVector;
    short status;
    std::string error;
    unsigned int errorRow;
    unsigned int errorColumn;

    void setError(const std::string &error, unsigned int errorRow, unsigned int errorColumn);

    Token& getCurrentToken();
    void moveToNextToken();

    TreeNode* parseProgram();
    TreeNode* parseClassDeclaration();
    TreeNode* parseClassName();
    TreeNode* parseMemberDeclaration();
    TreeNode* parseVariableDeclaration();
    TreeNode* parseMethodDeclaration();
    TreeNode* parseParameters();
    TreeNode* parseParameterDeclaration();
    TreeNode* parseBody();
    TreeNode* parseConstructorDeclaration();
    TreeNode* parseStatement();
    TreeNode* parseAssignment();
    TreeNode* parseWhileLoop();
    TreeNode* parseIfStatement();
    TreeNode* parseReturnStatement();
    TreeNode* parseExpression();
    TreeNode* parseArguments();
    TreeNode* parseName();
    TreeNode* parsePrimary();
};


#endif //PROJECT_PARSER_H
