#ifndef PROJECT_TREENODE_H
#define PROJECT_TREENODE_H

#include <vector>
#include "parser.h"
#include "../lexical/lexer.h"
#include "../semantical/visitors/BaseVisitor.h"

//class BaseVisitor;

class TreeNode {
private:
    NodeType nodeType;
    std::vector<TreeNode*> children;
    Token token;
    unsigned int rowInSource;
    unsigned int columnInSource;
public:
    TreeNode(NodeType nodeType);
    TreeNode(NodeType nodeType, unsigned int rowInSource, unsigned int columnInSource);

    NodeType getNodeType() const;
    const std::vector<TreeNode*> &getChildren() const;
    void appendChild(TreeNode* child);
    const Token &getToken() const;
    unsigned int getRowInSource() const;
    unsigned int getColumnInSource() const;

    void setToken(const Token &token);
    void deleteChildren();
    std::string toJSON();

    std::string getTypeString() const;

    void accept(BaseVisitor *visitor);
};


#endif //PROJECT_TREENODE_H
