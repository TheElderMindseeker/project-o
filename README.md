# Project O Compiler

This is user guide for Project O Compiler.  Project O Compiler translates
OLanguage into Project O assembly code which can be executed using Project O
Virtual Machine, POVM.  Language specification can be found in `/docs` folder.
POVM is a separate project, that is, Project O assembly interpreter written in
Python.

## Compilation

### Prerequisities

To compile the Project O Compiler you will need:

*  Any modern (C++11) cpp compiler
*  CMake >= 3.7.2
*  GNU Make

### How-to

To compile the project use the following commands.  We assume that your working
directory prior to executing these commands has `/project-o` as its
subdirectory and Project O Compiler code is situated in `/project-o` folder.

```bash
mkdir build && cd ./build
cmake ../project-o
make
```

Make will produce two targets: `cpo` and `test_cpo`.  `cpo` is the compiler
itself.  `test_cpo` is for testing purposes, use it on your own risk.

## Execution

After you compiled the Project O Compiler, you can compile programs written in
OLanguage.  To do so, use the following command (given you have got `cpo`):

```bash
cpo <input-file-name> <output-file-name>
```

For example:

```bash
cpo input.cpo output.povm
```

## Recommended File Extensions

For OLanguage: `.cpo`

For Project O Assembly: `.povm`