# Semantical Structure

The file describes semantical structure of Project O Language.  It also
describes some ideas that drive the development of the compiler for the
language.

## Built-Ins

The Project O Language requires that some classes are available in all programs
written in the language.  These classes are considered built-in.  The classes
are `Class`, `AnyValue`, `Integer`, `Real`, `Boolean`, `AnyRef`, `Array[T]`,
and `List[T]`.

As these classes are needed in all programs in the language, as they have
well-defined interfaces, but no obligatory implementation, they will be added
to each program as AST tree pseudonodes with predefined implementation in C++.

## Classes

All the classes of the program reside in one and only namespace.  This means
the user cannot create two or more classes with the same name.  This also
implies that the user cannot create class which has the same name as one of
the built-in classes, because they are considered to be in the namespace
before the start of the compilation process.

The namespace of the program is universal, which means that it doesn't matter
if the class is declared before its first use.  For example, the following code
is semantically correct:

```
class One is
    var TwoInst : Two
end

class Two is
end
```

### LLVM IR

According to [1] and my own considerations, each class will have two parts in
LLVM IR: structural part and functions definition.  Structural part will use
LLVM's struct type to define the members (variables) of the class instance and
the members of all of its ancestors, and the functions definition part will
provide functions to work with class instances.  Each function will have form
CLASSNAME_FUNCNAME_POSTFIX and will be global, as class names are unique
accross the program, this will ensure the uniqueness of function names.

## Generics

There is a support for generic types in Project O language.  This compiler only
implements one-level generics (so no nested generics are allowed).  An attempt
to create nested generic will lead to a compilation error.  When generic type
is first used in the program with the actual parameter substituted, the
compiler creates the implementation of the template with the actual parameter.
This leads to the creation of new record in class table.  The name for
instanciated class is formed in the following way:
`GENERIC_CLASSNAME$ACTUAL_PARAMETER_CLASSNAME`, so the `List[T]` class
instanciated with `T = Integer` will be `List$Integer` in class table.

## Inheritance

In OLanguage each class (except for `Class`) must have a parent.  When a class
B extends class A, class B acquires all the methods of A, but none of its
members.  This means that A members are hidden from the class B and B's
functions cannot access them.  On the other hand, all functions in OLanguage
are considered virtual, and all functions of the class are accessible by that
class successors.

There is no 'super' keyword in OLanguage, so to access parent's functions the
class needs to call them without `this` keyword.  So the call without `this`
keyword will be considered to be a call to parent class function.

It is said that all classes are situated in the same namespace and so it would
have been likely that user can extend from class that is not yet defined.  But
unlike class usage, class inheritance requires strict ordering and so it is
impossible to inherit from class that is defined further in the program than
its successor.

The parent of the class must be, in fact, a class.  This means that user cannot
extend class from a generic.  This makes all generics become leaves in
inheritance tree.

### Implementation

#### Compilation

When the generic class is parsed it is not added to the class table (see
Classes section).  But when the generic class with actual generic parameters
is used first time, it is, in fact, created and added to the class table with
all its generic parameters resolved.

## Class Members

There are three types of class members: variables, methods, and constructors.
Variables are data of the class: each instance of the class acquires its own
unique copies of each variable defined in the class.  Methods are the
algorithms capable of working with instace data fields.  Methods belong to the
class itself, not the instances, but the methods can be called only by
accessing it through class instance.  In that case method is called and it
acquires all arguments passed to it by the call plus implicit parameter - the
instance itself, which can be accessed by `this` keyword.

Variables of the instace are accessible on write only inside the methods of
that instance class and are not accessible from outside.  In other words, all
instance variables are private.  On the other hand, all class methods are
public and can be accessed by anyone.

Although variables are not accessible from outside, their value can be read
as if the variable was, in fact, a property.  In other words, all instance
variables are read-only from outside the class.

### Implementation

#### Compilation

For each class the compiler builds the list of its members during Class Imaging
pass:

```
Integer     -> [ Integer$ctor$2, Integer$ctor$3, Integer$Min, ... ]
Real        -> [ Real$ctor$2, Real$ctor$3, Real$Min, Real$Max, ... ]
...
```

Whenever access operation is applied to instance, the compiler checks whether
this operation is valid by consulting the member list of that instance class.
The compiler also checks if it is valid to access the member from the scope
from which it is tried to access, so it would be possible to access instance
variable from inside that instance class method, but impossible from outside.

The methods in OLanguage can be overloaded, see Methods section.

## Variables

All variables in the OLanguage are just references to the real objects.
Although some types are considered to be value-types, they are in fact just
immutable types, which makes them the same, but leaves the implementation
consistent.

### Implementation

#### LLVM IR

All "real" objects are always created in stack frame of current function when
the constructor is called, or they must emerge as a result of Expression.

## Methods

Each class can have methods, which are just algorithms for processing data
stored inside the class instances.  Methods can be overloaded.  In other words
there can be two methods in a single class with same names, but different
formal parameters.  All methods in OLanguage are virtual.

### Implementation

#### LLVM IR

Each method maps to a global function named CLASSNAME_FUNCNAME_POSTFIX, where
CLASSNAME is the name of the class in which the method is defined, FUNCNAME is
the name of the function, and POSTFIX is the function unique signature created
using the method's formal parameters.  For each parameter in order of their
declaration (from left to right), find this parameter's class and class ID.
Join class IDs using underscore symbol (\_).

Example:

```
class MyClass is
    method MyMethod(a: Integer, b: Real, c: Integer, d: Boolean) is
        ...
    end
end
```

In this example MyMethod will have the following name as a global function in
LLVM IR:

```
MyClass$MyMethod$2.3.2.4
```

All methods in OLanguage are virtual.  To handle this, each class must have
virtual method table, called vtable.  This is according to [1] and my own
knowledge.  The pointer to vtable is stored in each of class instances.  When
the method of the instance is called, vtable address is extracted from instance
itself, method pointer is extracted from the table, and the method is called.

Vtable is created in LLVM IR module as a global variable of struct type.  It
consists of pointers to all the methods defined by the corresponding class.
Each class instance is initialized by the pointer to the vtable, not accessible
by the programmer.

## Constructors

Constructors in the classes are declared using `this` keyword.  As with methods
instance constructors can be overloaded with different formal parameters.
Constructors are called by writing Class Name and providing the list of actual
arguments (as in method call).  The new object is then created and given to
the constructor as implicit parameter accessible by `this` keyword.

### Implementation

#### LLVM IR

From the IR point of view, constructors don't differ from any other function.
The main difference lies in naming policy.  The base name of the constructor
is always `ctor`.

The trick is that when constructor is called in the OLanguage, the compiler
generates object-creation code prior to the ctor call and when the ctor is
called it acquires the newly created object as its implicit `this` argument.

## References

[1] https://mapping-high-level-constructs-to-llvm-ir.readthedocs.io