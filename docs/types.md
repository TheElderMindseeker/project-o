# Type Specification

The file describes the type specification for the Project O language.

## Available Types

There are three basic value types in the Project O language: Integer, Real, and
Boolean.

### Integer

Integer values in Project O language are represented by 64-bit signed integers.

### Real

Real values in Project O language are represented by 64-bit floating point
numbers according to IEEE 754.

### Boolean

Boolean values in Project O language are represented by 64-bit signed integers.
All values except 0 are considered True.  The value of 0 is considered False.
The implementation is encouraged, but not obliged, to use only positive 1 to
represent True.