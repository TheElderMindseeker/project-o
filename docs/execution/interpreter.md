# Interpreter

This document describes the input file format for the interpreter of the
compiled OLanguage.  The aim of compiled OLanguage is to be a very high level
assembler.

## Registers

The interpreter has got a special register for expression results called
`@result` and virtually infinite number of argument registers, that is,
registers for passing arguments to functions.  Argument registers have the form
`$NUM` where NUM is the number of register (starting, of course, from 0).  All
registers hold references to the objects, not objects themselves.

## Metadata

There are two types of metadata: vtables (virtual methods tables) and class
layouts (layout of variables in object of a class).  Inside the input file
first all vtables must come, than all class layouts must come, than all
function implementations.

### Vtables

Vtables have the following format:

```
vtable ClassName
  methodName$signature: @className$methodName$signature
  ...
```

The first column of table is the symbolic method name common across inheritance
hierarchy and second column is the name of actual implementation of the method.

### Layouts

Class Layouts have the following format:

```
layout ClassName
  className$varName: varType
  ...
```

The first column of the table is the name of the variable, the second is the
variable type.

### Labels

Labels have the following syntax:

```
%labelName:
```

Labels are local to the function, that is, `%label1` in function
`@MyClass$myMethod$1.1.1` will not be conflicting with same label in
`@OtherClass$myMethod$1.1.1`.  This also means that jumps work only within
scope of current method.

## Commands

Here is the list of commands supported by interpreter.  Commands are legal only
within scope of the function.  Function definitions have the following syntax:

```
function @className$methodName$signature
#defineVars param1, param2, param3, ...
  command1
  command2
%label1:
  command3
  command4
  ...
```

### Literal

Syntax:

```
LITERAL Integer|Real|Boolean, <literal>
```

This command creates the object of Integer, Real or Boolean type from literal
and puts the reference to newly created object into `@result`.

### Put

Syntax:

```
PUT $NUM|@result, localVar|className$varName|this
```

This command puts the reference specified by second operand into the specified
register.

### Assign

Syntax:

```
ASSIGN localVar|className$varName, localVar|className$varName|@result
```

This command assigns the reference specified by the right-side to the
variable, specified by left side.  If the reference is assigned to localVar
and localVar does not exist, it is created right away.

### New

Syntax:

```
NEW className$signature
```

This command creates new object using specified constructor and saves reference
to it to @result.

### Call

Syntax:

```
CALL localVar|className$varName|this, methodName$signature
```

This command makes a virtual call to the function specified by
methodName$signature.  The interpreter chooses the function using vtable of the
object given as the first argument.  The arguments for the function are taken
from argument registers.

### Return

Syntax:

```
RETURN
```

This command returns the control from the current function into the enclosing
function.

### Jump

Syntax:

```
JUMP %label
```

This command makes unconditional jump to the specified label.

### Jump If

Syntax:

```
JUMPIF %label
```

This command makes conditional jump to the specified label.  If the object in
@result evaluates to True, the jump is taken, otherwise it is not.