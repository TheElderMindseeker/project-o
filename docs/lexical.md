# Lexical Structure

The file describes lexical structure of the Project O language.  It also
intorduces some ideas concerning lexical analysis of the language by Project O
Scanner (OScanner from here and below).

## Token List

This is full list of tokens of Project O language.

### Keywords

*  class
*  else
*  end
*  extends
*  if
*  is
*  loop
*  method
*  return
*  then
*  this
*  var
*  while

### Delimiters

*  Parentheses `(` and `)`
*  Square brackets `[` and `]`
*  Colon `:`
*  Comma `,`

### Operators

*  Assignment `:=`
*  Member access `.`

### Names

Names are special type of token that has no particular lexem to represent it.
There are several rules that Names are bound to.

1.  Each name must conform to regular expression `[a-zA-Z_][a-zA-Z0-9_]{0,255}`
2.  Keywords could not be used as Names
3.  Boolean literals (`True`, `False`) could not be used as Names

### Literals

There are three types of literals, conforming to three basic types in language
itself: Integer, Real, and Boolean.

1.  Integer: `[0-9]+`
2.  Real: `([0-9]+(\.[0-9]*)?)|(\.[0-9]+)`
3.  Boolean: `True|False`

NB: Read `types.md` for type specification.  Non-conformance to type limits is
considered to be lexical error.

## Implementation

The lexical analyzer should define the following enum for token types:

```cxx
enum TokenType {
    TK_CLASS,
    TK_ELSE,
    TK_END,
    TK_EXTENDS,
    TK_IF,
    TK_IS,
    TK_LOOP,
    TK_METHOD,
    TK_RETURN,
    TK_THEN,
    TK_THIS,
    TK_VAR,
    TK_WHILE,

    TK_LEFT_PAREN,
    TK_RIGHT_PAREN,
    TK_LEFT_BRACK,
    TK_RIGHT_BRACK,
    TK_COLON,
    TK_COMMA,

    TK_ASSIGNMENT,
    TK_ACCESS,

    TK_NAME,

    TK_INTEGER,
    TK_REAL,
    TK_BOOLEAN,

    TK_ENDMARKER
}
```

The lexical analyzer should also define the following struct for tokens:

```cxx
struct Token {
    TokenType type;

    std::string str_value;
    long long int int_value;
    double real_value;
}
```

According to its role, lexical analyzer should provide access to the token
stream in any suitable way.  The struct `Token` defines the token type and may
define one of values, depenging on the token's type.  The values are provided
according to the following mapping:

*  TK_NAME has `str_value`
*  TK_INTEGER has `int_value`
*  TK_REAL has `real_value`
*  TK_BOOLEAN has `int_value`