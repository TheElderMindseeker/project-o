# Class Structure

This document describes the second pass of semantical analysis of Project O
Compiler, called Class Structure, during which the compiler parses the contents
of the classes, that is, their variables, methods, and constructors.

## Variables Table

Each class must have a variable table associated with it after the class
structure pass.  The table has the following form:

| Name       | Expression |
|------------|------------|
| VariableA  | 0x40006745 |
| MyVariable | 0x405034A8 |
| ...        | ...        |

The table has two columns in it: Name and Expression.  In Name column the
string identifier of the variable is stored.  In Expression the pointer to
AST Node with Expression generating the variable is stored.

## Methods Table

Each class must have a methods table associated with it.  In methods table all
methods of the class, except for constructors are stored.  The table has the
following form:

| Name       | Parameters         | Return Value | Body       |
|------------|--------------------|--------------|------------|
| MyMethod1  | (Integer, Real)    | Real         | 0x40501FAA |
| AddNumbers | (Real, Real, Real) | Real         | 0x40BC5E77 |
| ...        | ...                | ...          | ...        |

There are 4 columns in the table denoting method's name, its parameters, return
value and pointer to AST Node with Body correspondingly.

## Constructors Table

Each class must have a constructors table associated with it.  In constructors
table all constructors of the class are stored.  The table has the following
form:

| Parameters         | Body       |
|--------------------|------------|
| (Boolean, Integer) | 0x4078AD6E |
| (Integer, Integer) | 0x4071A6C2 |
| ...                | ...        |

There are 2 columns in the table. First is Parameters and it describes the set
of formal parameters of the constructor.  The second is Body and it is a
pointer to the AST Node with constructor's body.

# Errors caught during Class Structure

*  Duplicate elements:
    -  Variables
    -  Methods
    -  Constructors
*  Parameters of non-existing types
*  Return values of non-existing types