# Class Imaging

This document describes the first pass of semantical analysis of Project O
Compiler, called Class Imaging.  During this step the compiler lists all
classes present in the program by creating two tables: Class Table for
concrete classes and Template Table for class templates.

## Class Table

The list of all the classes during compilation is implemented via class table
with two columns.  Note, that each class has unique ID inside the table, which
is equal to its index in the table:

| Class Name | Parent Class |
|------------|--------------|
| Class      | Class        |
| AnyValue   | Class        |
| Integer    | AnyValue     |
| ...        | ...          |

Built-in classes always have the same IDs:

0.  Class
1.  AnyValue
2.  Integer
3.  Real
4.  Boolean
5.  AnyRef

When the new class is created by the user, it is added to the class table.
If that class has no explicit predecessor, its parent class is considered to
be `Class`.  As can be seen from the table, `Class`'s parent is itself.  This
is done to the marker of the end of the inheritance list:

| Class Name | Parent Class |
|------------|--------------|
| Class      | Class        |
| AnyValue   | Class        |
| Integer    | AnyValue     |
| ...        | ...          |
| UserClass  | Class        |

## Template Table

The list of template classes during compilation is implemented via template
table with three columns: Class Name and Parent Class, Generic Name.  If the
compiler encounters generic class during Class Imaging step it doesn't add
this class to class table but rather to template table.

| Class Name  | Parent Class | Generic Name |
|-------------|--------------|--------------|
| Array       | AnyRef       | T            |
| List        | AnyRef       | T            |
| ...         | ...          | ...          |
| UserGeneric | Class        | E            |

Classes `Array[T]` and `List[T]` are placed into the table before the start of
the compilation process, because they are predefined generics.

# Errors caught during Class Imaging

*  Duplicate classes
*  Duplicate templates
*  Impossible inheritance:
    -  Inheritance from template class (even with actual parameter)
    -  Inheritance from successor
    -  Inheritance from non-existing class