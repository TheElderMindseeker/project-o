# Syntactical Structure

The file describes the syntax of the Project O language.

The starting nonterminal symbol is `Program`

## EBNF

```
Program ::= { ClassDeclaration }

ClassDeclaration ::= 'class' ClassName [ extends NAME ] 'is'
    { MemberDeclaration }
'end'

ClassName ::= NAME [ '[' NAME ']' ]

MemberDeclaration ::=
    VariableDeclaration
    | MethodDeclaration
    | ConstructorDeclaration

VariableDeclaration ::= 'var' NAME ':' Expression

MethodDeclaration ::= 'method' NAME [ Parameters ] [ ':' ClassName ]
    'is' Body 'end'

Parameters ::= '(' ParameterDeclaration { ',' ParameterDeclaration } ')'

ParameterDeclaration ::= NAME ':' ClassName

Body ::= { VariableDeclaration | Statement }

ConstructorDeclaration ::= 'this' [ Parameters ] 'is' Body 'end'

Statement ::=
    Assignment
    | WhileLoop
    | IfStatement
    | ReturnStatement

Assignment ::= NAME ':=' Expression

WhileLoop ::= 'while' Expression 'loop' Body 'end'

IfStatement ::= 'if' Expression 'then' Body [ 'else' Body ] 'end'

ReturnStatement ::= 'return' [ Expression ]

Expression ::= Primary { '.' NAME [ Arguments ] }

Arguments ::= '(' Expression { ',' Expression } ')'

Primary ::=
    INTEGER
    | REAL
    | BOOLEAN
    | 'this'
    | ClassName [ Arguments ]
```

### List of Terminals

*  All keywords wrapped in ''
*  Basic types: `INTEGER`, `REAL`, `BOOLEAN`
*  Names, denoted by `NAME`

### List of Nonterminals

*  Program
*  ClassDeclaration
*  ClassName
*  MemberDeclaration
*  VariableDeclaration
*  MethodDeclaration
*  Parameters
*  ParameterDeclaration
*  Body
*  ConstructorDeclaration
*  Statement
*  Assignment
*  WhileLoop
*  IfStatement
*  ReturnStatement
*  Expression
*  Arguments
*  Primary

## BNF

**NOT SUPPORTED**

```
Program ::= ClassList

ClassList ::=
    ClassDeclaration ClassList
    | ''

ClassDeclaration ::= 'class' ClassName Extends 'is' MemberList 'end'

ClassName ::=
    NAME Generic

Generic ::=
    '[' ClassName ']'
    | ''

Extends ::=
    'extends' ClassName
    | ''

MemberList ::=
    MemberDeclaration MemberList
    | ''

MemberDeclaration ::=
    VariableDeclaration
    | MethodDeclaration
    | ConstructorDeclaration

VariableDeclaration ::= 'var' NAME ':' Expression

MethodDeclaration ::= MethodSignature MethodBody

MethodSignature ::= 'method' NAME Parameters ReturnType

ReturnType ::=
    ':' NAME
    | ''

MethodBody ::= 'is' Body 'end'

Parameters ::=
    '(' ParameterList ')'
    | ''

ParameterList ::=
    ParameterDeclaration ParameterList
    | ''

ParameterDeclaration ::= NAME ':' ClassName

Body ::=
    VariableDeclaration Body
    | Statement Body
    | ''

ConstructorDeclaration ::= 'this' Parameters MethodBody

Statement ::=
    Assignment
    | WhileLoop
    | IfStatement
    | ReturnStatement

Assignment ::= NAME ':=' Expression

WhileLoop ::= 'while' Expression 'loop' Body 'end'

IfStatement ::= 'if' Expression 'then' Body ElseCase 'end'

ElseCase ::=
    'else' Body
    | ''

ReturnStatement ::=
    'return'
    | 'return' Expression

Expression ::=
    Primary AccessList

AccessList ::=
    '.' NAME Arguments AccessList
    | ''

Arguments ::=
    '(' Expression ExpressionList ')'
    | ''

ExpressionList ::=
    ',' Expression ExpressionList
    | ''

Primary ::=
    INTEGER
    | REAL
    | BOOLEAN
    | 'this'
    | ClassName Arguments
```

### List of Terminals

*  All keywords wrapped in ''
*  Basic types: `INTEGER`, `REAL`, `BOOLEAN`
*  Names, denoted by `NAME`

### List of Nonterminals

*  Program
*  ClassList
*  ClassDeclaration
*  ClassName
*  Generic
*  Extends
*  MemberList
*  MemberDeclaration
*  VariableDeclaration
*  MethodDeclaration
*  MethodSignature
*  ReturnType
*  MethodBody
*  Parameters
*  ParameterList
*  ParameterDeclaration
*  Body
*  ConstructorDeclaration
*  Statement
*  Assignment
*  WhileLoop
*  IfStatement
*  ElseCase
*  ReturnStatement
*  Expression
*  AccessList
*  Arguments
*  ExpressionList
*  Primary