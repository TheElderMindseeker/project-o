#include "ImagingVisitor.h"
#include "../../syntactical/parser.h"
#include "../../lexical/lexer.h"
#include "../helpers/ClassDecalrationHelper.h"

ImagingVisitor::ImagingVisitor(ClassTable *classTable) {
    this->classTable = classTable;
    this->templateTable = new TemplateTable();
}

void ImagingVisitor::visitProgram(TreeNode *node) {
    if (node->getNodeType() == PE_PROGRAM) {
        for (auto classDeclaration : node->getChildren()) {
            if (classDeclaration->getNodeType() == PE_CLASS_DECLARATION) {
                classDeclaration->accept(this);
            }
        }
    }
};

void ImagingVisitor::visitClassDeclaration(TreeNode *pNode) {
    std::string className = ClassDecalrationHelper::getClassName(pNode);
    std::string parentName = ClassDecalrationHelper::getParentName(pNode);
    std::string generic = ClassDecalrationHelper::getGenericName(pNode);

    if (classTable->isClassDefined(className)) {
        hasError = true;
        error += "class was already defined in class table: "
                 + className + "\n";
    } else if (templateTable->isClassDefined(className)) {
        hasError = true;
        error += "class was already defined in template table: "
                 + className + "\n";
    } else if (! parentName.empty()
               && ! classTable->isClassDefined(parentName)) {
        hasError = true;
        error += "parent wasn't defined in class table: " + className + "\n";
    } else if (! parentName.empty() && className == parentName) {
        hasError = true;
        error += "class extends himself: " + className + "\n";
    } else {
        if (generic.empty()) {
            if (parentName.empty()) {
                classTable->insertClass(className);
            } else {
                classTable->insertClass(className, parentName);
            }
        } else {
            if (parentName.empty()) {
                templateTable->insertClass(className, generic);
            } else {
                templateTable->insertClass(className, generic, parentName);
            }
        }
    }
};

const std::string &ImagingVisitor::getErrorDescription() {
    return error;
}

bool ImagingVisitor::errorStatus() {
    return hasError;
}