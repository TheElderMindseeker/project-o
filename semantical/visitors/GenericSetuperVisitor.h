#ifndef GENERIC_SETUPER_VISITOR_H
#define GENERIC_SETUPER_VISITOR_H

#include "BaseVisitor.h"

#include <string>

class TreeNode;

class GenericSetuperVisitor : public BaseVisitor {
public:
    void visitProgram(TreeNode *node) override {};
    void visitClassDeclaration(TreeNode *node) override;
    void visitClassName(TreeNode *node) override;
    void visitMemberDeclaration(TreeNode *node) override;
    void visitVariableDeclaration(TreeNode *node) override;
    void visitMethodDeclaration(TreeNode *node) override;
    void visitParameters(TreeNode *node) override;
    void visitParameterDeclaration(TreeNode *node) override;
    void visitBody(TreeNode *node) override;
    void visitConstructorDeclaration(TreeNode *node) override;
    void visitStatement(TreeNode *node) override;
    void visitAssignment(TreeNode *node) override;
    void visitWhileLoop(TreeNode *node) override;
    void visitIfStatement(TreeNode *node) override;
    void visitReturnStatement(TreeNode *node) override;
    void visitExpression(TreeNode *node) override;
    void visitArguments(TreeNode *node) override;
    void visitName(TreeNode *node) override;
    void visitPrimary(TreeNode *node) override;

    bool errorStatus() const override;
    const std::string &getErrorDescription() const override;

    void setActualParameter(const std::string &actualParameter);

    TreeNode *getSetupClass();

private:
    bool isGeneric(TreeNode *classNameNode);

    TreeNode *createClassNameNode(TreeNode *oldClassNameNode);
    TreeNode *copyClassNameNode(TreeNode *oldClassNameNode);
    TreeNode *substituteClassNameNode(TreeNode *oldClassNameNode);
    TreeNode *createNameNodeWithName(TreeNode *oldClassNameNode,
                                     const std::string &newName);

    TreeNode *copyNameNode(TreeNode *oldNameNode);

    bool error;
    std::string errorDescription;

    std::string genericParameter;
    std::string actualParameter;

    TreeNode *setupClass = nullptr;
    TreeNode *newNode = nullptr;
};

#endif // GENERIC_SETUPER_VISITOR_H