//
// Created by Andrey on 03.12.2018.
//

#ifndef PROJECT_O_COMPILER_TYPEDEDUCINGVISITOR_H
#define PROJECT_O_COMPILER_TYPEDEDUCINGVISITOR_H


#include <map>
#include "BaseVisitor.h"
#include "ClassStructureVisitor.h"
#include "../tables/ClassTable.h"

class TypeDeducingVisitor : public BaseVisitor {
public:
    TypeDeducingVisitor(const ClassTable *classTable, const std::map<uint32_t, ClassStructure *> &classStructures);

    void visitProgram(TreeNode *node) override;
    void visitClassDeclaration(TreeNode *node) override;
    void visitMethodDeclaration(TreeNode *node) override;
    void visitParameters(TreeNode *node) override;
    void visitBody(TreeNode *node) override;
    void visitConstructorDeclaration(TreeNode *node) override;
    void visitAssignment(TreeNode *node) override;
    void visitWhileLoop(TreeNode *node) override;
    void visitIfStatement(TreeNode *node) override;
    void visitReturnStatement(TreeNode *node) override;
    void visitExpression(TreeNode *node) override;
    void visitArguments(TreeNode *node) override;

    bool errorStatus() const override;
    const std::string &getErrorDescription() const override;

    std::map<TreeNode *, std::pair<uint32_t, std::string>> *getExpressionTypesMap() const;

private:
    bool hasError;
    std::string errorDescription;

    const ClassTable* classTable;
    std::map<uint32_t, ClassStructure *> classStructures;

    uint32_t contextClassId;
    std::string contextClassName;
    std::map<std::string, TreeNode*>* contextClassVars;
    std::string contextMethodName;
    std::vector<std::map<std::string, TreeNode*>*>* contextMethodVars;
    std::map<std::string, std::pair<uint32_t, std::string>>* contextMethodParams;
    std::vector<std::string>* contextMethodParamsVect;
    bool visitingClassVarDeclarations;

    std::map<TreeNode*, std::pair<uint32_t, std::string>>* expressionTypesMap;

    void parseMethodOrConstructor(TreeNode* node);
    std::pair<uint32_t, std::string> getPrimaryType(const TreeNode* node);
    std::pair<uint32_t, std::string> getCallResultType(
            uint32_t baseClassId,
            const std::string& methodName,
            const TreeNode* arguments,
            const TreeNode* methodNameNode,
            const std::string& baseClassName);
    std::string getNameFromClassNameNode(const TreeNode* node) const;
    bool conforms(std::string typeA, std::string typeB);
};


#endif //PROJECT_O_COMPILER_TYPEDEDUCINGVISITOR_H
