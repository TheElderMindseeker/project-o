#include "ClassSorterVisitor.h"

#include "../../syntactical/TreeNode.h"

#include <utility>
#include <exception>
#include <sstream>

void ClassSorterVisitor::visitProgram(TreeNode *node) {
    try {
        // Program is just a list of class declarations
        for (TreeNode *child : node->getChildren()) {
            child->accept(this);
        }
    } catch (std::logic_error &e) {
        error = true;
        errorDescription = e.what();
    }
}

void ClassSorterVisitor::visitClassDeclaration(TreeNode *node) {
    auto &children = node->getChildren();

    // First child is always ClassName node
    auto classNameNode = children.at(0);
    // Get the name of the class
    auto &className = classNameNode->getChildren().at(0)->getToken().str_value;

    // If className node has more than 1 child, it is generic class
    if (classNameNode->getChildren().size() > 1) {
        auto inserted = templates.insert(
            std::make_pair(className, node)
        ).second;

        if (! inserted) {
            std::stringstream reason;
            reason << "Duplicate template " << className
                   << " at " << " unknown";
            throw std::logic_error(reason.str());
        }
    } else {
        auto inserted = classes.insert(
            std::make_pair(className, node)
        ).second;

        if (! inserted) {
            std::stringstream reason;
            reason << "Duplicate template " << className
                   << " at " << " unknown";
            throw std::logic_error(reason.str());
        }
    }
}

const std::map<std::string, TreeNode *> &
        ClassSorterVisitor::getClasses() const {
    return classes;
}

const std::map<std::string, TreeNode *> &
        ClassSorterVisitor::getTemplates() const {
    return templates;
}

bool ClassSorterVisitor::errorStatus() const {
    return error;
}

const std::string &ClassSorterVisitor::getErrorDescription() const {
    return errorDescription;
}