#ifndef CLASS_STRUCTURE_VISITOR_H
#define CLASS_STRUCTURE_VISITOR_H

#include <map>
#include <vector>
#include <string>

#include "BaseVisitor.h"

class VariablesTable;
class MethodsTable;
class ConstructorsTable;
class ClassTable;
class GenericMagician;
class TreeNode;

struct ClassStructure {
    std::string *className;
    VariablesTable *variablesTable;
    MethodsTable *methodsTable;
    ConstructorsTable *ctorsTable;
};

class ClassStructureVisitor : public BaseVisitor {
public:
    ClassStructureVisitor(const ClassTable *classTable,
                          const GenericMagician *genericMagician);

    void visitProgram(TreeNode *node) override;
    void visitClassDeclaration(TreeNode *node) override;
    void visitMemberDeclaration(TreeNode *node) override;
    void visitVariableDeclaration(TreeNode *node) override;
    void visitMethodDeclaration(TreeNode *node) override;
    void visitConstructorDeclaration(TreeNode *node) override;

    const std::map<uint32_t, ClassStructure *> &getStructures() const;

    bool errorStatus() const override;
    const std::string &getErrorDescription() const override;

private:
    void addStandardLibrary();
    void addClassMethods();
    void addClassCtors();
    void addAnyValueMethods();
    void addAnyValueCtors();
    void addIntegerMethods();
    void addIntegerCtors();
    void addRealMethods();
    void addRealCtors();
    void addBooleanMethods();
    void addBooleanCtors();
    void addAnyRefMethods();
    void addAnyRefCtors();
    void addArrayMethods(const std::string &actualParam);
    void addArrayCtors(const std::string &actualParam);
    void addListMethods(const std::string &actualParam);
    void addListCtors(const std::string &actualParam);

    void setCurrentClass(TreeNode *classNameNode);
    const std::string &getName(TreeNode *nameNode);
    bool isGeneric(TreeNode *classNameNode);
    const std::vector<std::string> *parseParameters(TreeNode *parameters);
    const std::string *parseReturnValue(TreeNode *classNameNode);

    std::map<uint32_t, ClassStructure *> classStructures;

    bool error = false;
    std::string errorDescription;

    const ClassTable *classTable;
    const GenericMagician *genericMagician;

    ClassStructure *current;
};

#endif // CLASS_STRUCTURE_VISITOR_H