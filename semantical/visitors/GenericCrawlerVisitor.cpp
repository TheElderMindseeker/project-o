#include "GenericCrawlerVisitor.h"

#include "../../syntactical/TreeNode.h"

#include <exception>
#include <utility>
#include <sstream>

GenericCrawlerVisitor::GenericCrawlerVisitor(
        const std::vector<std::string> &classNames,
        const std::vector<std::string> &templateNames) {
    // First of all, add stdlib classes
    classes.insert("Class");
    classes.insert("AnyValue");
    classes.insert("Integer");
    classes.insert("Real");
    classes.insert("Boolean");
    classes.insert("AnyRef");

    for (auto &name : classNames) {
        classes.insert(name);
    }
    for (auto &name : templateNames) {
        userSetups.insert(std::make_pair(name, std::set<std::string>()));
    }
}

void GenericCrawlerVisitor::visitProgram(TreeNode *node) {
    try {
        for (TreeNode *child : node->getChildren()) {
            child->accept(this);
        }
    } catch (std::logic_error &e) {
        error = true;
        errorDescription = e.what();
    }
}

void GenericCrawlerVisitor::visitClassDeclaration(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitClassName(TreeNode *node) {
    if (usage && isGeneric(node)) {
        auto &children = node->getChildren();
        // First child is the name of template itself
        auto templateName = getName(children.at(0));
        // Second child is the name of its actual parameter
        auto paramName = getName(children.at(1));

        if (classes.count(paramName) == 0) {
            std::stringstream reason;
            reason << "Usage of unknown class " << paramName
                   << " as a generic parameter"
                   << " at " << " unknown";
            throw std::logic_error(reason.str());
        }

        if (templateName == "Array") {
            arraySetups.insert(paramName);
        } else if (templateName == "List") {
            listSetups.insert(paramName);
        } else if (userSetups.count(templateName) == 0) {
            std::stringstream reason;
            reason << "Usage of unknown template " << templateName
                   << " at " << " unknown";
            throw std::logic_error(reason.str());
        } else {
            userSetups.at(templateName).insert(paramName);
        }
    }
}

void GenericCrawlerVisitor::visitMemberDeclaration(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitVariableDeclaration(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitMethodDeclaration(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        if (child->getNodeType() == PE_CLASS_NAME)
            usage = true;
        child->accept(this);
        usage = false;
    }
}

void GenericCrawlerVisitor::visitParameters(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitParameterDeclaration(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        if (child->getNodeType() == PE_CLASS_NAME)
            usage = true;
        child->accept(this);
        usage = false;
    }
}

void GenericCrawlerVisitor::visitBody(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitConstructorDeclaration(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitStatement(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitAssignment(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitWhileLoop(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitIfStatement(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitReturnStatement(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitExpression(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitArguments(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitName(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void GenericCrawlerVisitor::visitPrimary(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        if (child->getNodeType() == PE_CLASS_NAME)
            usage = true;
        child->accept(this);
        usage = false;
    }
}

const std::map<std::string, std::set<std::string>> &
        GenericCrawlerVisitor::getUserSetups() const {
    return userSetups;
}

const std::set<std::string> &GenericCrawlerVisitor::getArraySetups() const {
    return arraySetups;
}

const std::set<std::string> &GenericCrawlerVisitor::getListSetups() const {
    return listSetups;
}

const std::string &GenericCrawlerVisitor::getName(TreeNode *nameNode) {
    // The name of the class must be stored in token's str_value
    return nameNode->getToken().str_value;
}

bool GenericCrawlerVisitor::isGeneric(TreeNode *classNameNode) {
    auto &children = classNameNode->getChildren();
    // The className node is considered to be for generic class
    // if it has second child (which must be ClassName)
    return children.size() > 1;
}

bool GenericCrawlerVisitor::errorStatus() const {
    return error;
}

const std::string &GenericCrawlerVisitor::getErrorDescription() const {
    return errorDescription;
}