#ifndef CLASS_SORTER_VISITOR_H
#define CLASS_SORTER_VISITOR_H

#include <string>
#include <map>

#include "BaseVisitor.h"

class TreeNode;

class ClassSorterVisitor : public BaseVisitor {
public:
    void visitProgram(TreeNode *node) override;
    void visitClassDeclaration(TreeNode *node) override;

    bool errorStatus() const override;
    const std::string &getErrorDescription() const override;

    const std::map<std::string, TreeNode *> &getClasses() const;
    const std::map<std::string, TreeNode *> &getTemplates() const;

private:
    bool error = 0;
    std::string errorDescription;

    std::map<std::string, TreeNode *> classes;
    std::map<std::string, TreeNode *> templates;
};

#endif // CLASS_SORTER_VISITOR_H