#include "GenericSetuperVisitor.h"

#include "../../syntactical/TreeNode.h"
#include "../../lexical/lexer.h"

void GenericSetuperVisitor::visitClassDeclaration(TreeNode *node) {
    setupClass = new TreeNode(node->getNodeType(),
                              node->getRowInSource(),
                              node->getColumnInSource());

    auto &children = node->getChildren();
    TreeNode *classNameNode = children.at(0);

    if (isGeneric(classNameNode)) {
        // Set generic parameter variable so other functions
        // know which generic to substitute with actual parameter
        auto genericNameNode = classNameNode->getChildren().at(1);
        genericParameter = genericNameNode->getToken().str_value;

        auto newClassNameNode = createClassNameNode(classNameNode);
        setupClass->appendChild(newClassNameNode);
    } else {
        // Set generic parameter to empty string in order not to
        // substitute anything by mistake
        genericParameter = "";

        auto cpyClassNameNode = copyClassNameNode(classNameNode);
        setupClass->appendChild(cpyClassNameNode);
    }

    for (TreeNode *child : children) {
        // ClassName in ClassDeclaration is special case
        if (child->getNodeType() != PE_CLASS_NAME) {
            child->accept(this);
            // Each child stores newly created node in newNode
            setupClass->appendChild(newNode);
        }
    }
}

void GenericSetuperVisitor::visitClassName(TreeNode *node) {
    if (isGeneric(node)) {
        newNode = substituteClassNameNode(node);
    } else {
        auto temp = new TreeNode(node->getNodeType(),
                                 node->getRowInSource(),
                                 node->getColumnInSource());

        for (TreeNode *child : node->getChildren()) {
            child->accept(this);
            temp->appendChild(newNode);
        }

        newNode = temp;
    }
}

void GenericSetuperVisitor::visitMemberDeclaration(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitVariableDeclaration(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitMethodDeclaration(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitParameters(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitParameterDeclaration(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitBody(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitConstructorDeclaration(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitStatement(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitAssignment(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitWhileLoop(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitIfStatement(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitReturnStatement(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitExpression(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitArguments(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitName(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());

    auto nameValue = node->getToken().str_value;
    if (nameValue == genericParameter) {
        // Substitute actual parameter for generic
        Token newToken {
            TK_NAME,
            actualParameter,
            0,
            0.0,
            node->getRowInSource(),
            node->getColumnInSource()
        };
        temp->setToken(newToken);
    } else {
        // The str_value must remain the same
        temp->setToken(node->getToken());
    }

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

void GenericSetuperVisitor::visitPrimary(TreeNode *node) {
    auto temp = new TreeNode(node->getNodeType(),
                             node->getRowInSource(),
                             node->getColumnInSource());
    // Preserve token which may hold important information
    temp->setToken(node->getToken());

    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
        temp->appendChild(newNode);
    }

    newNode = temp;
}

bool GenericSetuperVisitor::errorStatus() const {
    return error;
}

const std::string &GenericSetuperVisitor::getErrorDescription() const {
    return errorDescription;
}

void GenericSetuperVisitor::setActualParameter(
    const std::string &actualParameter
) {
    this->actualParameter = actualParameter;
}

TreeNode *GenericSetuperVisitor::getSetupClass() {
    return setupClass;
}

bool GenericSetuperVisitor::isGeneric(TreeNode *classNameNode) {
    auto &children = classNameNode->getChildren();
    // The className node is considered to be for generic class
    // if it has second child (which must be ClassName)
    return children.size() > 1;
}

TreeNode *GenericSetuperVisitor::createClassNameNode(
    TreeNode *oldClassNameNode
) {
    auto oldNameNode = oldClassNameNode->getChildren().at(0);
    auto oldName = oldNameNode->getToken().str_value;
    return createNameNodeWithName(oldClassNameNode,
                                  oldName + "$" + actualParameter);
}

TreeNode *GenericSetuperVisitor::copyClassNameNode(
    TreeNode *oldClassNameNode
) {
    auto oldNameNode = oldClassNameNode->getChildren().at(0);
    auto oldName = oldNameNode->getToken().str_value;
    return createNameNodeWithName(oldClassNameNode, oldName);
}

TreeNode *GenericSetuperVisitor::substituteClassNameNode(
    TreeNode *oldClassNameNode
) {
    auto oldNameNode = oldClassNameNode->getChildren().at(0);
    auto oldName = oldNameNode->getToken().str_value;
    auto oldGenericNode = oldClassNameNode->getChildren().at(1);
    auto oldGeneric = oldGenericNode->getToken().str_value;
    return createNameNodeWithName(oldClassNameNode,
                                  oldName + "$" + oldGeneric);
}

TreeNode *GenericSetuperVisitor::createNameNodeWithName(
    TreeNode *oldClassNameNode,
    const std::string &newName
) {
    auto newClassNameNode = new TreeNode(
        oldClassNameNode->getNodeType(),
        oldClassNameNode->getRowInSource(),
        oldClassNameNode->getColumnInSource()
    );

    auto oldNameNode = oldClassNameNode->getChildren().at(0);

    auto newNameNode = new TreeNode(
        oldNameNode->getNodeType(),
        oldNameNode->getRowInSource(),
        oldNameNode->getColumnInSource()
    );

    // Set the new name for class
    Token newToken {
        TK_NAME,
        newName,
        0,
        0.0,
        oldNameNode->getRowInSource(),
        oldNameNode->getColumnInSource()
    };
    newNameNode->setToken(newToken);

    newClassNameNode->appendChild(newNameNode);

    return newClassNameNode;
}