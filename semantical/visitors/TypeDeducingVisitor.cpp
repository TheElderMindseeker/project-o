#include <utility>

#include <string>
#include <sstream>
#include <map>
#include "TypeDeducingVisitor.h"
#include "../../syntactical/TreeNode.h"
#include "../../lexical/lexer.h"
#include "../tables/MethodsTable.h"
#include "../tables/VariablesTable.h"

TypeDeducingVisitor::TypeDeducingVisitor(const ClassTable *classTable,
                                         const std::map<uint32_t, ClassStructure *> &classStructures) :
                                         classTable(classTable),
                                         classStructures(classStructures) {}

void TypeDeducingVisitor::visitProgram(TreeNode *node) {
    hasError = false;
    expressionTypesMap = new std::map<TreeNode*, std::pair<uint32_t, std::string>>();
    for (const auto& chClass : node->getChildren()) {
        chClass->accept(this);
    }
}

void TypeDeducingVisitor::visitClassDeclaration(TreeNode *node) {
    if (hasError) {
        return;
    }

    contextClassName = getNameFromClassNameNode(node->getChildren().at(0));
    contextClassId = static_cast<uint32_t>(classTable->getClassId(contextClassName));
    contextClassVars = new std::map<std::string, TreeNode*>();
    visitingClassVarDeclarations = true;
    for (const auto& member : node->getChildren()) {
        // NAME nodes does not have children so we need
        // to process this as special case
        if (member->getNodeType() == PE_NAME)
            continue;
        if (member->getChildren().at(0)->getNodeType() == PE_VARIABLE_DECLARATION) {
            TreeNode* varName = member->getChildren().at(0)->getChildren().at(0);
            TreeNode* varExpr = member->getChildren().at(0)->getChildren().at(1);
            varExpr->accept(this);
            contextClassVars->insert(
                    std::pair<std::string, TreeNode*>(varName->getToken().str_value, varExpr));
        }
    }
    visitingClassVarDeclarations = false;
    for (const auto& member : node->getChildren()) {
        if (!member->getChildren().empty() &&
                member->getChildren().at(0)->getNodeType() == PE_METHOD_DECLARATION) {
            member->getChildren().at(0)->accept(this);
        } else if (!member->getChildren().empty() &&
                    member->getChildren().at(0)->getNodeType() == PE_CONSTRUCTOR_DECLARATION) {
            TreeNode* temp = member->getChildren().at(0);
            temp->accept(this);
        }
    }
    delete contextClassVars;
}

void TypeDeducingVisitor::visitMethodDeclaration(TreeNode *node) {
    if (hasError) {
        return;
    }
    contextMethodName = node->getChildren().at(0)->getToken().str_value;
    parseMethodOrConstructor(node);
}

void TypeDeducingVisitor::visitConstructorDeclaration(TreeNode *node) {
    if (hasError) {
        return;
    }
    parseMethodOrConstructor(node);
}

void TypeDeducingVisitor::parseMethodOrConstructor(TreeNode *node) {
    contextMethodParams = new std::map<std::string, std::pair<uint32_t, std::string>>();
    contextMethodVars = new std::vector<std::map<std::string, TreeNode*>*>();
    contextMethodParamsVect = new std::vector<std::string>();
    // For constructor
    if (node->getChildren().at(0)->getNodeType() == PE_PARAMETERS) {
        node->getChildren().at(0)->accept(this);
    }
    // For method
    if (node->getChildren().size() > 1 &&
            node->getChildren().at(1)->getNodeType() == PE_PARAMETERS) {
        node->getChildren().at(1)->accept(this);
    }

    node->getChildren().back()->accept(this);

    delete contextMethodParamsVect;
    delete contextMethodParams;
    delete contextMethodVars;
    contextMethodVars = nullptr;
}

void TypeDeducingVisitor::visitParameters(TreeNode *node) {
    if (hasError) {
        return;
    }
    std::string parameterName, className;
    uint32_t classId;
    std::pair<uint32_t, std::string> classPair;
    for (const auto& parameter : node->getChildren()) {
        parameterName = parameter->getChildren().at(0)->getToken().str_value;
        className = getNameFromClassNameNode(parameter->getChildren().at(1));
        classId = static_cast<uint32_t>(classTable->getClassId(className));
        classPair = std::pair<uint32_t, std::string>(classId, className);
        contextMethodParams->insert(
                std::pair<std::string, std::pair<uint32_t, std::string>>(parameterName, classPair));
        contextMethodParamsVect->push_back(className);
    }
}

void TypeDeducingVisitor::visitBody(TreeNode *node) {
    if (hasError) {
        return;
    }
    contextMethodVars->push_back(new std::map<std::string, TreeNode*>());
    for (const auto& entry : node->getChildren()) {
        if (entry->getNodeType() == PE_VARIABLE_DECLARATION) {
            TreeNode* varName = entry->getChildren().at(0);
            TreeNode* varExpr = entry->getChildren().at(1);
            varExpr->accept(this);
            contextMethodVars->back()->insert(
                    std::pair<std::string, TreeNode*>(varName->getToken().str_value, varExpr));
        } else if (entry->getNodeType() == PE_STATEMENT) {
            entry->getChildren().at(0)->accept(this);
        }
    }
    delete contextMethodVars->back();
    contextMethodVars->pop_back();
}

void TypeDeducingVisitor::visitAssignment(TreeNode *node) {
    if (hasError) {
        return;
    }
    // ToDo Assignment type checking can be in this method
    // Just let visitExpression do its job
    node->getChildren().at(1)->accept(this);
    std::string varName = node->getChildren().at(0)->getToken().str_value;

    if (!visitingClassVarDeclarations) {
        for (int i = contextMethodVars->size(); i > 0; --i) {
            if (contextMethodVars->at(i-1)->count(varName) > 0) {
                TreeNode* sourceExpression = contextMethodVars->at(i-1)->at(varName);
                std::string varType = expressionTypesMap->at(sourceExpression).second;
                std::string exprType = expressionTypesMap->at(node->getChildren().at(1)).second;
                if (!conforms(exprType, varType)) {
                    hasError = true;
                    std::stringstream errorStream;
                    errorStream << "[" << node->getRowInSource() << ":"
                        << node->getColumnInSource() << "] Type mismatch. "
                        << "Expected " << varType << " got " << exprType << ".";
                    errorDescription = errorStream.str();
                    return;
                }
            }
        }
        // Look at method parameters
        if (contextMethodParams->count(varName) > 0) {
            hasError = true;
            std::stringstream errorStream;
            errorStream << "[" << node->getRowInSource() << ":"
                << node->getColumnInSource() << "] Do not try to reassign input argumetns!";
            errorDescription = errorStream.str();
            return;
        }
    }

}

void TypeDeducingVisitor::visitWhileLoop(TreeNode *node) {
    if (hasError) {
        return;
    }
    // First of all place conditional expression in the map
    node->getChildren().at(0)->accept(this);
    // Then run visitBody
    node->getChildren().at(1)->accept(this);
}

void TypeDeducingVisitor::visitIfStatement(TreeNode *node) {
    if (hasError) {
        return;
    }
    // First of all place conditional expression in the map
    node->getChildren().at(0)->accept(this);
    // Then run visitBody on then-statement
    node->getChildren().at(1)->accept(this);
    // And run visitBody on else-statement, if it exists
    if (node->getChildren().size() == 3) {
        node->getChildren().at(2)->accept(this);
    }
}

void TypeDeducingVisitor::visitReturnStatement(TreeNode *node) {
    if (hasError) {
        return;
    }
    // Simply place expression in the map
    if (!node->getChildren().empty()) {
        node->getChildren().at(0)->accept(this);
    }

    std::string reqRetType = classStructures
            .at(contextClassId)->methodsTable
            ->getReturnType(contextMethodName, *contextMethodParamsVect);
    if (reqRetType == "") {
        reqRetType = "nothing";
    }
    std::string actRetType;
    if (node->getChildren().empty()) {
        actRetType = "nothing";
    } else {
        actRetType = expressionTypesMap
                ->at(node->getChildren().at(0)).second;
    }

    if (!conforms(actRetType, reqRetType)) {
        hasError = true;
        std::stringstream errorStream;
        errorStream << "[" << node->getRowInSource() << ":"
            << node->getColumnInSource() << "] Return type mismatch. "
            << "Expected " << reqRetType << " got " << actRetType << ".";
        errorDescription = errorStream.str();
        return;
    }
}

void TypeDeducingVisitor::visitExpression(TreeNode *node) {
    if (hasError) {
        return;
    }
    std::pair<uint32_t, std::string> primaryType =
            getPrimaryType(node->getChildren().at(0));

    if (node->getChildren().size() > 1) {
        std::pair<uint32_t, std::string> currentType = primaryType;
        std::string methodName;
        TreeNode* methodNameNode;
        TreeNode* arguments = nullptr;
        for (int i = 1; i < node->getChildren().size(); ++i) {
            methodNameNode = node->getChildren().at(i);
            methodName = methodNameNode->getToken().str_value;
            if (i+1 < node->getChildren().size() &&
                    node->getChildren().at(i+1)->getNodeType() == PE_ARGUMENTS) {
                i++;
                arguments = node->getChildren().at(i);
                arguments->accept(this);
            } else {
                arguments = nullptr;
            }
            currentType = getCallResultType(currentType.first, methodName, arguments, methodNameNode, currentType.second);
        }
        expressionTypesMap->insert(
                std::pair<TreeNode*, std::pair<uint32_t, std::string>>(node, currentType));
    } else {
        expressionTypesMap->insert(
                std::pair<TreeNode*, std::pair<uint32_t, std::string>>(node, primaryType));
    }
}

std::pair<uint32_t, std::string> TypeDeducingVisitor::getPrimaryType(const TreeNode *node) {
    if (node->getChildren().empty()) {
        // This is either this or some primitive type
        switch (node->getToken().type) {
            case TK_INTEGER:
                return std::pair<uint32_t, std::string>(2, "Integer");
            case TK_REAL:
                return std::pair<uint32_t, std::string>(3, "Real");
            case TK_BOOLEAN:
                return std::pair<uint32_t, std::string>(4, "Boolean");
            case TK_THIS:
                return std::pair<uint32_t, std::string>(contextClassId, contextClassName);
            default:
                hasError = true;
                errorDescription = "[" + std::to_string(node->getRowInSource()) + ":" +
                        std::to_string(node->getColumnInSource())
                        + "] This is neither primitive type nor 'this'";
                return std::pair<uint32_t, std::string>();
        }
    } else {
        // This is actually a class name or a variable
        std::string tempName = getNameFromClassNameNode(node->getChildren().at(0));
        // Look at local variables
        if (!visitingClassVarDeclarations) {
            for (int i = contextMethodVars->size(); i > 0; --i) {
                if (contextMethodVars->at(i-1)->count(tempName) > 0) {
                    TreeNode* expression = contextMethodVars->at(i-1)->at(tempName);
                    return expressionTypesMap->at(expression);
                }
            }
            // Look at method parameters
            if (contextMethodParams->count(tempName) > 0) {
                return contextMethodParams->at(tempName);
            }
        }
        // Look at class member variables
        if (contextClassVars->count(tempName) > 0) {
            TreeNode* expression = contextClassVars->at(tempName);
            return expressionTypesMap->at(expression);
        }
        // Look at classes
        if (classTable->isClassDefined(tempName)) {
            if (node->getChildren().size() > 1) {
                node->getChildren().at(1)->accept(this);
            }
            return std::pair<uint32_t, std::string>(
                    classTable->getClassId(tempName), tempName);
        }
        // If we are still in the method, something went wrong...
    }
}

std::pair<uint32_t, std::string> TypeDeducingVisitor::getCallResultType(
        uint32_t baseClassId,
        const std::string &methodName,
        const TreeNode *arguments,
        const TreeNode *methodNameNode,
        const std::string& baseClassName) {
    std::vector<std::string> vectorArguments;
    if (arguments != nullptr) {
        for (TreeNode* argument : arguments->getChildren()) {
            vectorArguments.push_back(expressionTypesMap->at(argument).second);
        }
    }
    MethodsTable* methodsTable = classStructures.at(baseClassId)->methodsTable;
    VariablesTable* variablesTable = classStructures.at(baseClassId)->variablesTable;
    std::string typeName;
    std::string tempName = baseClassName;
    // Try to find method considening inheritance
    while (! methodsTable->has(methodName, vectorArguments) // While method not exists
            && tempName != "Class") { // Or we've reached base class
        tempName = classTable->getParentName(tempName); // Get name of parent
        methodsTable = classStructures.at(classTable->getClassId(tempName))->methodsTable;
    }
    if (tempName != "Class") {
        // This is class method
        typeName = methodsTable->getReturnType(methodName, vectorArguments);
    } else if (variablesTable->has(methodName)){
        TreeNode* variable = const_cast<TreeNode*>(variablesTable->get(methodName));
        typeName = expressionTypesMap->at(variable).second;
    } else {
        // Something went it is neither method nor variable
        hasError = true;
        errorDescription = "[" + std::to_string(methodNameNode->getRowInSource()) + ":" +
                std::to_string(methodNameNode->getColumnInSource()) +
                "] Method or variable does not exist";
        return std::pair<uint32_t, std::string>();
    }
    uint32_t typeId = static_cast<uint32_t>(classTable->getClassId(typeName));
    return std::pair<uint32_t, std::string>(typeId, typeName);
}

void TypeDeducingVisitor::visitArguments(TreeNode *node) {
    if (hasError) {
        return;
    }
    for (const auto& expression : node->getChildren()) {
        expression->accept(this);
    }
}

std::string TypeDeducingVisitor::getNameFromClassNameNode(const TreeNode *node) const {
    TreeNode* nodeClassName = node->getChildren().at(0);
    std::string stringClassName = nodeClassName->getToken().str_value;
    return stringClassName;
}

bool TypeDeducingVisitor::errorStatus() const {
    return hasError;
}

const std::string &TypeDeducingVisitor::getErrorDescription() const {
    return errorDescription;
}

std::map<TreeNode *, std::pair<uint32_t, std::string>> *TypeDeducingVisitor::getExpressionTypesMap() const {
    return expressionTypesMap;
}

bool TypeDeducingVisitor::conforms(std::string typeA, std::string typeB) {
    if (typeA == "nothing" || typeB == "nothing") {
        return false;
    }
    bool conforms = false;
    std::string tempType = std::move(typeA);
    while (tempType != "Class" and !conforms) {
        if (tempType == typeB) {
            conforms = true;
        }
        tempType = classTable->getParentName(tempType);
    }
    return conforms;
}

