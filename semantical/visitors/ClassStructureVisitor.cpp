#include "ClassStructureVisitor.h"

#include <exception>
#include <utility>
#include <sstream>

#include "../tables/VariablesTable.h"
#include "../tables/MethodsTable.h"
#include "../tables/ConstructorsTable.h"
#include "../tables/ClassTable.h"
#include "../tables/TemplateTable.h"
#include "../../syntactical/TreeNode.h"
#include "../generics/GenericMagician.h"

ClassStructureVisitor::ClassStructureVisitor(
    const ClassTable *classTable, const GenericMagician *genericMagician)
        : classTable(classTable), genericMagician(genericMagician) {
    addStandardLibrary();
}

void ClassStructureVisitor::visitProgram(TreeNode *node) {
    try {
        for (TreeNode *child : node->getChildren()) {
            child->accept(this);
        }
    } catch (const std::exception &e) {
        error = true;
        errorDescription = e.what();
    }
}

void ClassStructureVisitor::visitClassDeclaration(TreeNode *node) {
    auto &children = node->getChildren();
    // The first child of ClassDeclaration is always ClassName
    setCurrentClass(children.at(0));

    for (TreeNode *child : children) {
        if (child->getNodeType() == PE_MEMBER_DECLARATION)
            child->accept(this);
    }
}

void ClassStructureVisitor::visitMemberDeclaration(TreeNode *node) {
    for (TreeNode *child : node->getChildren()) {
        child->accept(this);
    }
}

void ClassStructureVisitor::visitVariableDeclaration(TreeNode *node) {
    auto &children = node->getChildren();

    // The first child of VariableDeclaration is its NAME
    auto &name = getName(children[0]);
    // The second is Expression node
    TreeNode *expression = children[1];

    bool inserted = current->variablesTable->insert(name, expression);

    if (!inserted) {
        std::stringstream reason;
        reason << "Duplicate variable " << name << " in class "
               << *current->className << " at " << " unknown";
        throw std::logic_error(reason.str());
    }
}

void ClassStructureVisitor::visitMethodDeclaration(TreeNode *node) {
    auto &children = node->getChildren();

    std::string name;
    const std::vector<std::string> *parameters = nullptr;
    const std::string *returnValue = nullptr;
    const TreeNode *body = nullptr;

    for (TreeNode *child : children) {
        if (child->getNodeType() == PE_NAME) {
            name = getName(child);
        } else if (child->getNodeType() == PE_PARAMETERS) {
            parameters = parseParameters(child);
        } else if (child->getNodeType() == PE_CLASS_NAME) {
            returnValue = parseReturnValue(child);
        } else if (child->getNodeType() == PE_BODY) {
            body = child;
        }
    }

    if (parameters == nullptr) {
        parameters = new std::vector<std::string>();
    }
    if (returnValue == nullptr) {
        returnValue = new std::string("");
    }

    bool inserted = current->methodsTable->insert(
            name, *parameters, *returnValue, body
    );

    delete parameters;
    delete returnValue;
    // Do NOT delete body: it will ruin the tree

    if (!inserted) {
        std::stringstream reason;
        reason << "Duplicate method " << name << " in class "
               << *current->className << " at " << " unknown";
        throw std::logic_error(reason.str());
    }
}

void ClassStructureVisitor::visitConstructorDeclaration(TreeNode *node) {
    auto &children = node->getChildren();

    const std::vector<std::string> *parameters = nullptr;
    const TreeNode *body = nullptr;

    for (TreeNode *child : children) {
        if (child->getNodeType() == PE_PARAMETERS) {
            parameters = parseParameters(child);
        } else if (child->getNodeType() == PE_BODY) {
            body = child;
        }
    }

    // If we haven't found parameters node then
    // we are dealing with empty parameter list
    if (parameters == nullptr) {
        parameters = new std::vector<std::string>();
    }

    bool inserted = current->ctorsTable->insert(*parameters, body);

    delete parameters;
    // Do NOT delete body: it will ruin the tree

    if (!inserted) {
        std::stringstream reason;
        reason << "Duplicate constructor of class "
               << *current->className << " at " << " unknown";
        throw std::logic_error(reason.str());
    }
}

const std::map<uint32_t, ClassStructure *> &
ClassStructureVisitor::getStructures() const {
    return classStructures;
}

bool ClassStructureVisitor::errorStatus() const {
    return error;
}

const std::string &ClassStructureVisitor::getErrorDescription() const {
    return errorDescription;
}

void ClassStructureVisitor::addStandardLibrary() {
    current = new ClassStructure();
    current->className = new std::string("Class");
    current->variablesTable = new VariablesTable();
    current->methodsTable = new MethodsTable(classTable);
    current->ctorsTable = new ConstructorsTable("Class", classTable);
    addClassMethods();
    addClassCtors();
    classStructures.insert(std::make_pair(classTable->getClassId("Class"),
                                          current));

    current = new ClassStructure();
    current->className = new std::string("AnyValue");
    current->variablesTable = new VariablesTable();
    current->methodsTable = new MethodsTable(classTable);
    current->ctorsTable = new ConstructorsTable("AnyValue", classTable);
    addAnyValueMethods();
    addAnyValueCtors();
    classStructures.insert(std::make_pair(classTable->getClassId("AnyValue"),
                                          current));

    current = new ClassStructure();
    current->className = new std::string("Integer");
    current->variablesTable = new VariablesTable();
    current->methodsTable = new MethodsTable(classTable);
    current->ctorsTable = new ConstructorsTable("Integer", classTable);
    addIntegerMethods();
    addIntegerCtors();
    classStructures.insert(std::make_pair(classTable->getClassId("Integer"),
                                          current));

    current = new ClassStructure();
    current->className = new std::string("Real");
    current->variablesTable = new VariablesTable();
    current->methodsTable = new MethodsTable(classTable);
    current->ctorsTable = new ConstructorsTable("Real", classTable);
    addRealMethods();
    addRealCtors();
    classStructures.insert(std::make_pair(classTable->getClassId("Real"),
                                          current));

    current = new ClassStructure();
    current->className = new std::string("Boolean");
    current->variablesTable = new VariablesTable();
    current->methodsTable = new MethodsTable(classTable);
    current->ctorsTable = new ConstructorsTable("Boolean", classTable);
    addBooleanMethods();
    addBooleanCtors();
    classStructures.insert(std::make_pair(classTable->getClassId("Boolean"),
                                          current));

    current = new ClassStructure();
    current->className = new std::string("AnyRef");
    current->variablesTable = new VariablesTable();
    current->methodsTable = new MethodsTable(classTable);
    current->ctorsTable = new ConstructorsTable("AnyRef", classTable);
    addAnyRefMethods();
    addAnyRefCtors();
    classStructures.insert(std::make_pair(classTable->getClassId("AnyRef"),
                                          current));

    for (auto &actualParam : genericMagician->getArraySetups()) {
        current = new ClassStructure();
        current->className = new std::string("Array$" + actualParam);
        current->variablesTable = new VariablesTable();
        current->methodsTable = new MethodsTable(classTable);
        current->ctorsTable = new ConstructorsTable("Array$" + actualParam,
                                                    classTable);
        addArrayMethods(actualParam);
        addArrayCtors(actualParam);

        classStructures.insert(std::make_pair(
            classTable->getClassId("Array$" + actualParam),
            current
        ));
    }

    for (auto &actualParam : genericMagician->getListSetups()) {
        current = new ClassStructure();
        current->className = new std::string("List$" + actualParam);
        current->variablesTable = new VariablesTable();
        current->methodsTable = new MethodsTable(classTable);
        current->ctorsTable = new ConstructorsTable("List$" + actualParam,
                                                    classTable);
        addListMethods(actualParam);
        addListCtors(actualParam);

        classStructures.insert(std::make_pair(
            classTable->getClassId("List$" + actualParam),
            current
        ));
    }
}

void ClassStructureVisitor::addClassMethods() {}

void ClassStructureVisitor::addClassCtors() {}

void ClassStructureVisitor::addAnyValueMethods() {}

void ClassStructureVisitor::addAnyValueCtors() {}

void ClassStructureVisitor::addIntegerMethods() {
    // toReal : Real
    current->methodsTable->insert(
        "toReal", {}, "Real", nullptr
    );
    // toBoolean : Boolean
    current->methodsTable->insert(
        "toBoolean", {}, "Boolean", nullptr
    );
    // UnaryMinus : Integer
    current->methodsTable->insert(
        "UnaryMinus", {}, "Integer", nullptr
    );
    // Plus(Integer) : Integer
    current->methodsTable->insert(
        "Plus", {"Integer"}, "Integer", nullptr
    );
    // Plus(Real) : Real
    current->methodsTable->insert(
        "Plus", {"Real"}, "Real", nullptr
    );
    // Minus(Integer) : Integer
    current->methodsTable->insert(
        "Minus", {"Integer"}, "Integer", nullptr
    );
    // Minus(Real) : Real
    current->methodsTable->insert(
        "Minus", {"Real"}, "Real", nullptr
    );
    // Mult(Integer) : Integer
    current->methodsTable->insert(
        "Mult", {"Integer"}, "Integer", nullptr
    );
    // Mult(Real) : Real
    current->methodsTable->insert(
        "Mult", {"Real"}, "Real", nullptr
    );
    // Div(Integer) : Integer
    current->methodsTable->insert(
        "Div", {"Integer"}, "Integer", nullptr
    );
    // Div(Real) : Real
    current->methodsTable->insert(
        "Div", {"Real"}, "Real", nullptr
    );
    // Rem(Integer) : Integer
    current->methodsTable->insert(
        "Rem", {"Integer"}, "Integer", nullptr
    );

    //Relations

    // Less(Integer) : Boolean
    current->methodsTable->insert(
        "Less", {"Integer"}, "Boolean", nullptr
    );
    // Less(Real) : Boolean
    current->methodsTable->insert(
        "Less", {"Real"}, "Boolean", nullptr
    );
    // LessEqual(Integer) : Boolean
    current->methodsTable->insert(
        "LessEqual", {"Integer"}, "Boolean", nullptr
    );
    // LessEqual(Real) : Boolean
    current->methodsTable->insert(
        "LessEqual", {"Real"}, "Boolean", nullptr
    );
    // Greater(Integer) : Boolean
    current->methodsTable->insert(
        "Greater", {"Integer"}, "Boolean", nullptr
    );
    // Greater(Real) : Boolean
    current->methodsTable->insert(
        "Greater", {"Real"}, "Boolean", nullptr
    );
    // GreaterEqual(Integer) : Boolean
    current->methodsTable->insert(
        "GreaterEqual", {"Integer"}, "Boolean", nullptr
    );
    // GreaterEqual(Real) : Boolean
    current->methodsTable->insert(
        "GreaterEqual", {"Real"}, "Boolean", nullptr
    );
    // Equal(Integer) : Boolean
    current->methodsTable->insert(
        "Equal", {"Integer"}, "Boolean", nullptr
    );
    // Equal(Real) : Boolean
    current->methodsTable->insert(
        "Equal", {"Real"}, "Boolean", nullptr
    );
}

void ClassStructureVisitor::addIntegerCtors() {
    // this(Integer)
    current->ctorsTable->insert({"Integer"}, nullptr);
    // this(Real)
    current->ctorsTable->insert({"Real"}, nullptr);
}

void ClassStructureVisitor::addRealMethods() {
    // toInteger : Integer
    current->methodsTable->insert(
        "toInteger", {}, "Integer", nullptr
    );
    // UnaryMinus : Real
    current->methodsTable->insert(
        "UnaryMinus", {}, "Real", nullptr
    );
    // Plus(Integer) : Real
    current->methodsTable->insert(
        "Plus", {"Integer"}, "Real", nullptr
    );
    // Plus(Real) : Real
    current->methodsTable->insert(
        "Plus", {"Real"}, "Real", nullptr
    );
    // Minus(Integer) : Real
    current->methodsTable->insert(
        "Minus", {"Integer"}, "Real", nullptr
    );
    // Minus(Real) : Real
    current->methodsTable->insert(
        "Minus", {"Real"}, "Real", nullptr
    );
    // Mult(Integer) : Real
    current->methodsTable->insert(
        "Mult", {"Integer"}, "Real", nullptr
    );
    // Mult(Real) : Real
    current->methodsTable->insert(
        "Mult", {"Real"}, "Real", nullptr
    );
    // Div(Integer) : Real
    current->methodsTable->insert(
        "Div", {"Integer"}, "Real", nullptr
    );
    // Div(Real) : Real
    current->methodsTable->insert(
        "Div", {"Real"}, "Real", nullptr
    );
    // Rem(Integer) : Real
    current->methodsTable->insert(
        "Rem", {"Integer"}, "Real", nullptr
    );

    //Relations

    // Less(Integer) : Boolean
    current->methodsTable->insert(
        "Less", {"Integer"}, "Boolean", nullptr
    );
    // Less(Real) : Boolean
    current->methodsTable->insert(
        "Less", {"Real"}, "Boolean", nullptr
    );
    // LessEqual(Integer) : Boolean
    current->methodsTable->insert(
        "LessEqual", {"Integer"}, "Boolean", nullptr
    );
    // LessEqual(Real) : Boolean
    current->methodsTable->insert(
        "LessEqual", {"Real"}, "Boolean", nullptr
    );
    // Greater(Integer) : Boolean
    current->methodsTable->insert(
        "Greater", {"Integer"}, "Boolean", nullptr
    );
    // Greater(Real) : Boolean
    current->methodsTable->insert(
        "Greater", {"Real"}, "Boolean", nullptr
    );
    // GreaterEqual(Integer) : Boolean
    current->methodsTable->insert(
        "GreaterEqual", {"Integer"}, "Boolean", nullptr
    );
    // GreaterEqual(Real) : Boolean
    current->methodsTable->insert(
        "GreaterEqual", {"Real"}, "Boolean", nullptr
    );
    // Equal(Integer) : Boolean
    current->methodsTable->insert(
        "Equal", {"Integer"}, "Boolean", nullptr
    );
    // Equal(Real) : Boolean
    current->methodsTable->insert(
        "Equal", {"Real"}, "Boolean", nullptr
    );
}

void ClassStructureVisitor::addRealCtors() {
    // this(Integer)
    current->ctorsTable->insert({"Integer"}, nullptr);
    // this(Real)
    current->ctorsTable->insert({"Real"}, nullptr);
}

void ClassStructureVisitor::addBooleanMethods() {
    // toInteger : Integer
    current->methodsTable->insert(
        "toInteger", {}, "Integer", nullptr
    );
    // Or(Boolean) : Boolean
    current->methodsTable->insert(
        "Or", {"Boolean"}, "Boolean", nullptr
    );
    // And(Boolean) : Boolean
    current->methodsTable->insert(
        "And", {"Boolean"}, "Boolean", nullptr
    );
    // Xor(Boolean) : Boolean
    current->methodsTable->insert(
        "Xor", {"Boolean"}, "Boolean", nullptr
    );
    // Not(Boolean) : Boolean
    current->methodsTable->insert(
        "Not", {}, "Boolean", nullptr
    );
}

void ClassStructureVisitor::addBooleanCtors() {
    // this(Boolean)
    current->ctorsTable->insert({"Boolean"}, nullptr);
}

void ClassStructureVisitor::addAnyRefMethods() {}

void ClassStructureVisitor::addAnyRefCtors() {}

void ClassStructureVisitor::addArrayMethods(const std::string &actualParam) {
    current->methodsTable->insert(
        "length", {}, "Integer", nullptr
    );
    current->methodsTable->insert(
        "get", {"Integer"}, actualParam, nullptr
    );
    current->methodsTable->insert(
        "set", {"Integer", actualParam}, "", nullptr
    );
}

void ClassStructureVisitor::addArrayCtors(const std::string &actualParam) {
    current->ctorsTable->insert({"Integer"}, nullptr);
}

void ClassStructureVisitor::addListMethods(const std::string &actualParam) {
    current->methodsTable->insert(
        "get", {}, actualParam, nullptr
    );
    current->methodsTable->insert(
        "head", {}, "", nullptr
    );
    current->methodsTable->insert(
        "next", {}, "", nullptr
    );
    current->methodsTable->insert(
        "prepend", {actualParam}, "", nullptr
    );
    current->methodsTable->insert(
        "tail", {}, "Boolean", nullptr
    );
}

void ClassStructureVisitor::addListCtors(const std::string &actualParam) {
    current->ctorsTable->insert({}, nullptr);
    current->ctorsTable->insert({actualParam}, nullptr);
    current->ctorsTable->insert({actualParam, "Integer"}, nullptr);
}

void ClassStructureVisitor::setCurrentClass(TreeNode *classNameNode) {
    auto &children = classNameNode->getChildren();

    // First node of the className node is always NAME
    auto &name = getName(children.at(0));
    uint32_t classId;

    // After the execution of the function `current` will hold the structure
    // for the class we will be working with soon
    current = new ClassStructure();
    current->className = new std::string(name);
    current->variablesTable = new VariablesTable();
    current->methodsTable = new MethodsTable(classTable);
    current->ctorsTable = new ConstructorsTable(name, classTable);

    classId = classTable->getClassId(name);
    classStructures.insert(std::make_pair(classId, current));
}

const std::string &ClassStructureVisitor::getName(TreeNode *nameNode) {
    // The name of the class must be stored in token's str_value
    return nameNode->getToken().str_value;
}

bool ClassStructureVisitor::isGeneric(TreeNode *classNameNode) {
    auto &children = classNameNode->getChildren();
    // The className node is considered to be for generic class
    // if it has second child (which must be ClassName)
    return children.size() > 1;
}

const std::vector<std::string> *ClassStructureVisitor::parseParameters(
        TreeNode *parameters
) {
    auto &children = parameters->getChildren();
    auto result = new std::vector<std::string>();

    for (TreeNode *param : children) {
        // Param type is stored in the second child of param declaration
        // Generics are not yet supported
        auto &paramDecl = param->getChildren();
        // Param type is just a ClassName
        auto &paramClassName = paramDecl.at(1)->getChildren();
        // The NAME of the class is stored as the first child of ClassName
        const std::string &name = getName(paramClassName.at(0));

        if (!classTable->isClassDefined(name)) {
            delete result;
            std::stringstream reason;
            reason << "Parameter of unknown class " << name
                   << " at " << " unknown";
            throw std::logic_error(reason.str());
        }

        result->push_back(name);
    }

    return result;
}

const std::string *ClassStructureVisitor::parseReturnValue(
        TreeNode *classNameNode
) {
    auto &children = classNameNode->getChildren();

    // The class NAME is always the first child of ClassName node
    const std::string &name = getName(children.at(0));

    if (!classTable->isClassDefined(name)) {
        std::stringstream reason;
        reason << "Return value of unknown class " << name
               << " at " << " unknown";
        throw std::logic_error(reason.str());
    }

    std::string *result = new std::string(name);
    return result;
}