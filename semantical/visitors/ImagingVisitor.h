

#ifndef PROJECT_O_COMPILER_IMAGINGVISITOR_H
#define PROJECT_O_COMPILER_IMAGINGVISITOR_H


#include "BaseVisitor.h"
#include "../tables/ClassTable.h"
#include "../tables/TemplateTable.h"
#include <string>

class ImagingVisitor : public BaseVisitor  {
public:
    void visitProgram(TreeNode *node) override;

    void visitClassDeclaration(TreeNode *node) override;

    bool errorStatus() const override {};
    const std::string &getErrorDescription() const override {};


    ImagingVisitor(ClassTable *classTable);

    virtual bool errorStatus();
    virtual const std::string &getErrorDescription();

private:
    ClassTable * classTable;
    TemplateTable * templateTable;
    std::string error = "";
    bool hasError = false;
};


#endif //PROJECT_O_COMPILER_IMAGINGVISITOR_H
