#ifndef BASE_VISITOR_H
#define BASE_VISITOR_H

#include <string>

class TreeNode;

class BaseVisitor {
public:
    virtual void visitProgram(TreeNode *node) {};
    virtual void visitClassDeclaration(TreeNode *node) {};
    virtual void visitClassName(TreeNode *node) {};
    virtual void visitMemberDeclaration(TreeNode *node) {};
    virtual void visitVariableDeclaration(TreeNode *node) {};
    virtual void visitMethodDeclaration(TreeNode *node) {};
    virtual void visitParameters(TreeNode *node) {};
    virtual void visitParameterDeclaration(TreeNode *node) {};
    virtual void visitBody(TreeNode *node) {};
    virtual void visitConstructorDeclaration(TreeNode *node) {};
    virtual void visitStatement(TreeNode *node) {};
    virtual void visitAssignment(TreeNode *node) {};
    virtual void visitWhileLoop(TreeNode *node) {};
    virtual void visitIfStatement(TreeNode *node) {};
    virtual void visitReturnStatement(TreeNode *node) {};
    virtual void visitExpression(TreeNode *node) {};
    virtual void visitArguments(TreeNode *node) {};
    virtual void visitName(TreeNode *node) {};
    virtual void visitPrimary(TreeNode *node) {};

    virtual bool errorStatus() const = 0;
    virtual const std::string &getErrorDescription() const = 0;

protected:
    BaseVisitor() = default;
    virtual ~BaseVisitor() = default;
};

#endif // BASE_VISITOR_H