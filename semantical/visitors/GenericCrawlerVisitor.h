#ifndef GENERIC_CRAWLER_VISITOR_H
#define GENERIC_CRAWLER_VISITOR_H

#include <set>
#include <map>
#include <vector>
#include <string>

#include "BaseVisitor.h"

class TreeNode;

class GenericCrawlerVisitor : public BaseVisitor {
public:
    GenericCrawlerVisitor(const std::vector<std::string> &classNames,
                          const std::vector<std::string> &templateNames);

    void visitProgram(TreeNode *node) override;
    void visitClassDeclaration(TreeNode *node) override;
    void visitClassName(TreeNode *node) override;
    void visitMemberDeclaration(TreeNode *node) override;
    void visitVariableDeclaration(TreeNode *node) override;
    void visitMethodDeclaration(TreeNode *node) override;
    void visitParameters(TreeNode *node) override;
    void visitParameterDeclaration(TreeNode *node) override;
    void visitBody(TreeNode *node) override;
    void visitConstructorDeclaration(TreeNode *node) override;
    void visitStatement(TreeNode *node) override;
    void visitAssignment(TreeNode *node) override;
    void visitWhileLoop(TreeNode *node) override;
    void visitIfStatement(TreeNode *node) override;
    void visitReturnStatement(TreeNode *node) override;
    void visitExpression(TreeNode *node) override;
    void visitArguments(TreeNode *node) override;
    void visitName(TreeNode *node) override;
    void visitPrimary(TreeNode *node) override;

    bool errorStatus() const override;
    const std::string &getErrorDescription() const override;

    const std::map<std::string, std::set<std::string>> &getUserSetups() const;
    const std::set<std::string> &getArraySetups() const;
    const std::set<std::string> &getListSetups() const;

private:
    const std::string &getName(TreeNode *nameNode);
    bool isGeneric(TreeNode *classNameNode);

    bool error = false;
    std::string errorDescription;

    // Controls the parsing of template setups during tree walk.
    // This thing is needed to parse generic classes instiation
    // only in certain places of a tree such as methods and
    // variable declarations.
    bool usage = false;

    // Non-generic classes available
    std::set<std::string> classes;

    // Mapping between template names and actual parameters for them
    std::map<std::string, std::set<std::string>> userSetups;
    // Separate mappings for library classes: they're special cases
    std::set<std::string> arraySetups;
    std::set<std::string> listSetups;
};

#endif // GENERIC_CRAWLER_VISITOR_H