#include "CodeOptimizer.h"
#include <vector>
#include <utility>

std::string CodeOptimizer::optimize(std::string code) {
    std::vector<std::pair<std::string, std::string>> replaces = {
            {"\n  RETURN\n  RETURN", "\n  RETURN"},
            {"\n  PUT @caller, @result\n  PUT @result, @caller", ""}
    };

    for (const auto &replacement : replaces) {
        size_t pos = code.find(replacement.first);

        // Repeat till end is reached
        while( pos != std::string::npos)
        {
            // Replace this occurrence of Sub String
            code.replace(pos, replacement.first.size(), replacement.second);
            // Get the next occurrence from the current position
            pos =code.find(replacement.first, pos + replacement.second.size());
        }
    }
    return code;
}
