#ifndef PROJECT_CLASSDECALRATIONHELPER_H
#define PROJECT_CLASSDECALRATIONHELPER_H


#include <string>
#include "../../syntactical/TreeNode.h"

class ClassDecalrationHelper {
public:
    /**
     * next methods accepts nodes with type PE_CLASS_DECLARATION and returns obvious stuff
     */
    static std::string getClassName(TreeNode * node);
    static std::string getParentName(TreeNode * node);
    static std::string getGenericName(TreeNode * node);

    static void checkType(TreeNode *pNode);

    static std::string getName(TreeNode *pNode);
};


#endif //PROJECT_CLASSDECALRATIONHELPER_H
