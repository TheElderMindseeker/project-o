
#include "TablesPrinter.h"
#include <vector>
#include <iostream>

std::string TablesPrinter::classTableToStr(ClassTable *const classTable) {
    std::string res;
    int max = 0;
    const std::vector<std::pair<std::string, uint32_t>> &classPairs = classTable->getClassPairs();
    for (const auto &classPair : classPairs) {
        if (classPair.first.length() > max) {
            max = classPair.first.length();
        }
    }

    res +="\n|" + symbolsSequence(max * 2 + 1, '-') +"|\n";
    for (const auto &classPair : classPairs) {
        res +='|' + classPair.first + symbolsSequence(max - classPair.first.size()) + '|';
        std::string str2 = classPairs[classPair.second].first;
        res += str2 + symbolsSequence(max - str2.size()) + "|\n";
    }

    res += "|" + symbolsSequence(max * 2 + 1, '-') + "|\n";
    return res;
}

std::string TablesPrinter::symbolsSequence(int n, char a) {
    std::string res;
    for (int i = 0; i < n; i++) {
        res += a;
    }
    return res;
}

std::string TablesPrinter::templatesTableToStr(TemplateTable *templateTable) {
    std::string res;
    int max_1 = 0, max_2 = 0, max_0 = 0;
    // class name, parent, generic name
    const std::vector<std::tuple<std::string, std::string, std::string>> &templates = templateTable->getTemplateTable();
    for (const auto &template_ : templates) {
        if (std::get<0>(template_).length() > max_0) {
            max_0 = std::get<0>(template_).length();
        }
        if (std::get<1>(template_).length() > max_1) {
            max_1 = std::get<1>(template_).length();
        }
        if (std::get<2>(template_).length() > max_2) {
            max_2 = std::get<2>(template_).length();
        }
    }

    res += "\n|" + symbolsSequence(max_0 + max_1 + max_2 + 2, '-') + "|\n";
    for (const auto &template_ : templates) {

        res += '|' + std::get<0>(template_) + symbolsSequence(max_0 - std::get<0>(template_).length());

        res += '|' + std::get<1>(template_) + symbolsSequence(max_1 - std::get<1>(template_).length());
        res += '|' + std::get<2>(template_) + symbolsSequence(max_2 - std::get<2>(template_).length());
        res += "|\n";
    }
    res += "|" + symbolsSequence(max_0 + max_1 + max_2 + 2, '-') + "|\n";
    return res;
}

std::string TablesPrinter::VTableToStr(VTable *vTable, ClassTable *classTable) {
    std::string str;
    for (int i =6; i< classTable->getClassPairs().size(); i++) {
        auto classPair = classTable->getClassPairs()[i];
        str.append("vtable ").append(classPair.first).append(" \n");
        for (auto classMethod : vTable->getTable().at(classPair.first)) {
            str.append("  ").append(classMethod.first).append(":").append(classMethod.second).append("\n");
        }
        if (!vTable->getTable().at(classPair.first).empty())
            str = str.substr(0, str.size()-1);
        str.append("\n\n");
    }
    return str;
}
