#include "ClassDecalrationHelper.h"

std::string ClassDecalrationHelper::getClassName(TreeNode *node) {
    checkType(node);
    return node->getChildren().at(0)->getChildren()
           .at(0)->getToken().str_value;
}

std::string ClassDecalrationHelper::getParentName(TreeNode *node) {
    checkType(node);

    // Class declaration have the structure (ClassName, [NAME,] {members})
    auto &classDeclarationChildren = node->getChildren();

    if (classDeclarationChildren.size() > 1
            && classDeclarationChildren.at(1)->getNodeType() == PE_NAME) {
        return classDeclarationChildren.at(1)->getToken().str_value;
    }

    return "";
}

std::string ClassDecalrationHelper::getGenericName(TreeNode *node) {
    checkType(node);

    // Class declaration have the structure (ClassName, [NAME,] {members})
    auto &classDeclarationChildren = node->getChildren();
    // ClassName must be present as first child
    // ClassName has the structure (NAME [, NAME])
    auto className = classDeclarationChildren.at(0);

    // The second child is the generic parameter name, if it is present
    if (className->getChildren().size() > 1
            && className->getChildren().at(1)->getNodeType() == PE_NAME) {
        return className->getChildren().at(1)->getToken().str_value;
    }

    return "";
}

void ClassDecalrationHelper::checkType(TreeNode *pNode) {
    if (pNode->getNodeType() != PE_CLASS_DECLARATION){
        throw std::invalid_argument(
            "class declaration helper received node with invalid type: "
        );
    }
}

std::string ClassDecalrationHelper::getName(TreeNode *pNode) {
    for (auto children : pNode->getChildren()){
        if (children->getNodeType() == PE_NAME) {
            return getName(children);
        }
    }
    return nullptr;
}
