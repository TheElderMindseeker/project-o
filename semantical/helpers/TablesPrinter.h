/*
 *
 */

#ifndef PROJECT_TABLESPRINTER_H
#define PROJECT_TABLESPRINTER_H


#include "../tables/ClassTable.h"
#include "../tables/TemplateTable.h"
#include "../tables/VTable.h"

class TablesPrinter {
public:
    static std::string classTableToStr(ClassTable *classTable);

    static std::string templatesTableToStr(TemplateTable * templateTable);

    static std::string symbolsSequence(int i, char=' ');

    /**
     * class table is needed to order map correctly
     * @param vTable
     * @param classTable
     * @return
     */
    static std::string VTableToStr(VTable * vTable, ClassTable *classTable);
};


#endif //PROJECT_TABLESPRINTER_H
