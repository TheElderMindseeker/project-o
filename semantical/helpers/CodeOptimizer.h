/*
 * helps to optimize generated "assembler" code
 *
 * for now does simple stuff
 */

#ifndef PROJECT_O_COMPILER_CODEOPTIMIZER_H
#define PROJECT_O_COMPILER_CODEOPTIMIZER_H

#include <string>

class CodeOptimizer {

public:
    static std::string optimize(std::string code);

};


#endif //PROJECT_O_COMPILER_CODEOPTIMIZER_H
