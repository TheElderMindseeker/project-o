#ifndef O_LANG_SEMANTICALANALYZER_H
#define O_LANG_SEMANTICALANALYZER_H

#include "tables/ClassTable.h"
#include "tables/TemplateTable.h"
#include "../syntactical/TreeNode.h"

class GenericMagician;

class SemanticalAnalyzer {
public:
    void parse(TreeNode * root);

    SemanticalAnalyzer(const GenericMagician *genericMagician);

    ClassTable *getClassTable() const;

    bool isHasError() const;

    const std::string &getErrorDescription() const;

private:
    ClassTable *classTable;
    bool hasError = false;
    std::string errorDescription;

};


#endif //O_LANG_SEMANTICALANALYZER_H
