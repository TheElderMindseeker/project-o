/**
 * works on assumption
 * what previous visitors in directory "semantical/visitors"
 * passed AST tree successfully
 *
 * generated code well be available in getResult function
 */

#ifndef PROJECT_O_COMPILER_CODEGENVISITOR_H
#define PROJECT_O_COMPILER_CODEGENVISITOR_H


#include "../visitors/BaseVisitor.h"
#include "../tables/ClassTable.h"
#include "../tables/ConstructorsTable.h"
#include "../tables/MethodsTable.h"
#include "../tables/VariablesTable.h"
#include "../tables/VTable.h"
#include "../visitors/TypeDeducingVisitor.h"
#include <string>
#include <map>
#include <vector>
#include <stack>

class CodeGenVisitor : public BaseVisitor {
public:
    virtual void visitProgram(TreeNode *node);
    virtual void visitClassDeclaration(TreeNode *node);
    virtual void visitClassName(TreeNode *node);
    virtual void visitVariableDeclaration(TreeNode *node) ;
    virtual void visitMethodDeclaration(TreeNode *node) ;
    virtual void visitParameters(TreeNode *node) ;
    virtual void visitBody(TreeNode *node) ;
    virtual void visitConstructorDeclaration(TreeNode *node) ;
    virtual void visitStatement(TreeNode *node) ;
    virtual void visitAssignment(TreeNode *node);
    virtual void visitWhileLoop(TreeNode *node) ;
    virtual void visitIfStatement(TreeNode *node);
    virtual void visitReturnStatement(TreeNode *node);
    virtual void visitExpression(TreeNode *node) ;
    virtual void visitArguments(TreeNode *node) ;
    virtual void visitName(TreeNode *node) ;
    virtual void visitPrimary(TreeNode *node);

    virtual bool errorStatus() const ;
    virtual const std::string &getErrorDescription() const;

    CodeGenVisitor(ClassTable *classTable, TypeDeducingVisitor *typeDeducingVisitor);

    virtual ~CodeGenVisitor();

    const std::string &getResult() const;

private:
    std::string result, error;
    bool hasError = false;
    ClassTable * classTable;
    TypeDeducingVisitor * typeDeducingVisitor;

    std::string getMethodName(TreeNode * node);

    bool classVarsProcessing = false; // determines if we now are processing variables of class
    std::string currentClassName; // name of class what is processed now
    std::map<std::string, uint32_t> localVarsTable; // maps name of variable to it's type (id)
    std::map<std::string, uint32_t> classVarsTable; // maps name of global variable to it's type (id)
    std::string getNodeName(TreeNode *pNode);

    std::string createConstructorCall(TreeNode *pNode, std::string basic_string);

    std::map<std::string, std::string> preConstructors; // maps name of the class to it's pre constructor's initializations
    std::string idString; // string what contains signature of method call

    int labelsId; // determines current id of labels
};


#endif //PROJECT_O_COMPILER_CODEGENVISITOR_H
