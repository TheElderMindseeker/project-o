#include "CodeGenVisitor.h"
#include "../../syntactical/TreeNode.h"
#include "../helpers/ClassDecalrationHelper.h"
#include "../../lexical/lexer.h"
#include "../../syntactical/parser.h"
#include "../visitors/ClassStructureVisitor.h"
#include "../helpers/TablesPrinter.h"
#include <vector>
#include <iostream>


void CodeGenVisitor::visitProgram(TreeNode *node) {
    if (node->getNodeType() == PE_PROGRAM) {
        for (auto classDeclaration : node->getChildren()) {
            if (classDeclaration->getNodeType() == PE_CLASS_DECLARATION) {
                try {
                    classDeclaration->accept(this);
                } catch (int e) {
                    return;
                }
            }
        }
    }
}

void CodeGenVisitor::visitClassDeclaration(TreeNode *node) {
    currentClassName = ClassDecalrationHelper::getClassName(node);

    /*
     * here code will be simply added outside of the methods
     * and then it will cut and added to the preConstructors map
     */
    uint32_t prevLength = result.length();
    classVarsProcessing = true;
    for (auto varDeclaration : node->getChildren()) {
        if (!varDeclaration->getChildren().empty() &&
        varDeclaration->getChildren()[0]->getNodeType() == PE_VARIABLE_DECLARATION) {
            varDeclaration->getChildren()[0]->accept(this);
        }
    }
    classVarsProcessing = false;
    preConstructors[currentClassName] = preConstructors[classTable->getParentName(currentClassName)]
            + result.substr(prevLength, result.size());

    result = result.substr(0, prevLength);

    for (auto constrDeclaration : node->getChildren()) {
        if (!constrDeclaration->getChildren().empty() &&
        constrDeclaration->getChildren()[0]->getNodeType() == PE_CONSTRUCTOR_DECLARATION) {
            constrDeclaration->getChildren()[0]->accept(this);
        }
    }

    for (auto methodDeclaration : node->getChildren()) {
        if (!methodDeclaration->getChildren().empty() &&
        methodDeclaration->getChildren()[0]->getNodeType() == PE_METHOD_DECLARATION) {
            methodDeclaration->getChildren()[0]->accept(this);
        }
    }

    classVarsTable.clear();
}

// PE_PRIMARY
/**
 * puts result of primary expression into the "@result" register
 * @param node
 */
 // Generally speaking I see here only 3 possibilities
 // 1 - some literal like Integer
 // 2 - name of variable
 // 3 - creation of new Object
void CodeGenVisitor::visitPrimary(TreeNode *node) {
    // no childs - some literal or 'this'
    if (node->getChildren().empty()) {
        switch (node->getToken().type){
            case TK_INTEGER:
                result += "  LITERAL Integer, " + std::to_string(node->getToken().int_value) + "\n";
                break;
            case TK_REAL:
                result += "  LITERAL Real, " + std::to_string(node->getToken().real_value) + "\n";
                break;
            case TK_BOOLEAN:
                result += "  LITERAL Boolean, " + std::to_string(node->getToken().int_value) + "\n";
                break;
            case TK_THIS:
                result += "  PUT @result, this\n";
                break;
            default:
                hasError = true;
                error = "invalid primary, current result: " + result;
                throw 1;
        }
    } else {
        std::string name = getNodeName(node);
        if (classTable->isClassDefined(name)) {
            result += createConstructorCall(node, name);
        } else if (localVarsTable.find(name) != localVarsTable.end()) {
            result += "  PUT @result, " + name + "\n";
        } else if (classVarsTable.find(name) != classVarsTable.end()) {
            result += "  PUT @result, " + currentClassName + "$" + name + "\n";
        } else {
            hasError = true;
            error += "unknown identifier " + name + " current result: " + result + "\n";
            throw 1;
        }
    }
}

std::string CodeGenVisitor::createConstructorCall(TreeNode *node, std::string name) {
    std::string res;
    // collecting data about input to the constructor
    if (node->getChildren().size() > 1) {
        node->getChildren()[1]->accept(this); // calling child with arguments
        res += "  NEW " + name + ", @" + name + "$" + idString + "\n";// signature of function will be in idString
    } else {
        res += "  NEW " + name + ", @" + name + "$" + "\n";
    }
    return res;
}

std::string CodeGenVisitor::getNodeName(TreeNode *pNode) {
    return pNode->getChildren()[0]->getChildren()[0]->getToken().str_value;
}

void CodeGenVisitor::visitClassName(TreeNode *node) {
    node->getChildren()[0]->accept(this);
}

void CodeGenVisitor::visitVariableDeclaration(TreeNode *node) {
    node->getChildren()[1]->accept(this); // processes expression inside the var declaration and puts result into
    auto currentNode = node->getChildren()[1];
    auto typesMap = typeDeducingVisitor->getExpressionTypesMap();


    if (typesMap->find(currentNode) == typesMap->end()) {
        hasError = true;
        error = "accessing argument not in the types map, row: " + std::to_string(currentNode->getRowInSource()) +
                " column: " + std::to_string(currentNode->getColumnInSource());
        throw 1;
    }

    uint32_t varType = typesMap->at(currentNode).first;

    result += "  ASSIGN ";
    std::string name = node->getChildren()[0]->getToken().str_value;
    if (classVarsProcessing) {
        classVarsTable[name] = varType;
        result += currentClassName + "$" + name;
    } else {
        localVarsTable[name] = varType;
        result += name;
    }
    result += ", @result\n";
}

// METHOD DECLARATION
/**
 * adds new line before function declaration
 * @param node
 */
void CodeGenVisitor::visitMethodDeclaration(TreeNode *node) {
    labelsId = 0;
    localVarsTable.clear();
    result += "\nfunction @" + currentClassName + "$" + getMethodName(node) + "$";
    //signature
    for (auto child : node->getChildren()) {
        if (child->getNodeType() == PE_PARAMETERS) {
            child->accept(this);
            break;
        }
    }
    result += "\n";
    for (auto child : node->getChildren()) {
        if (child->getNodeType() == PE_BODY) {
            child->accept(this);
            break;
        }
    }
    result += "  RETURN\n";
}


std::string CodeGenVisitor::getMethodName(TreeNode *node) {
    if (node->getNodeType() == PE_METHOD_DECLARATION) {
        for (auto child : node->getChildren()) {
            if (child->getNodeType() == PE_NAME) {
                return child->getToken().str_value;
            }
        }
    }
    throw "failed getMethodName() in code generation, current result: " + result;
}

/**
 * adds:
 * id's of parameters inside node separated by '.'
 * "#defineVars "
 * names of variables
 * @param node
 */
void CodeGenVisitor::visitParameters(TreeNode *node) {
    std::vector<std::string> varNames;
    for (auto child : node->getChildren()) {
        std::string varName = child->getChildren()[0]->getToken().str_value;
        varNames.push_back(varName);
        std::string paramClass = child->getChildren()[1]->getChildren()[0]->getToken().str_value;
        result += std::to_string(classTable->getClassId(paramClass)) + '.';
        localVarsTable[varName] = static_cast<unsigned int>(classTable->getClassId(paramClass));
    }
    result = result.substr(0, result.size()-1) + "\n";
    result += "#defineVars ";
    for (const auto &varName: varNames) {
        result += varName + ", ";
    }
    result = result.substr(0, result.size()-2);
}

void CodeGenVisitor::visitBody(TreeNode *node) {
    for (auto child : node->getChildren()) {
        if (child->getNodeType() == PE_VARIABLE_DECLARATION || child->getNodeType() == PE_STATEMENT) {
            child->accept(this);
        }
    }
}

void CodeGenVisitor::visitConstructorDeclaration(TreeNode *node) {
    result += "\nfunction @" + currentClassName + "$";
    //signature
    for (auto child : node->getChildren()) {
        if (child->getNodeType() == PE_PARAMETERS) {
            child->accept(this);
            break;
        }
    }
    result += "\n";
    if (preConstructors.find(currentClassName) != preConstructors.end()) {
        result += preConstructors[currentClassName];
    }
    for (auto child : node->getChildren()) {
        if (child->getNodeType() == PE_BODY) {
            child->accept(this);
            break;
        }
    }
    result += "  PUT @result, this\n  RETURN\n";
}

void CodeGenVisitor::visitStatement(TreeNode *node) {
    node->getChildren()[0]->accept(this);
}

void CodeGenVisitor::visitAssignment(TreeNode *node) {
    node->getChildren()[1]->accept(this);
    result += "  ASSIGN ";
    std::string name = node->getChildren()[0]->getToken().str_value;
    if (classVarsTable.find(name) != classVarsTable.end()) {
        result += currentClassName + "$";
    }
    result += name + ", @result\n";
}

void CodeGenVisitor::visitWhileLoop(TreeNode *node) {
    int curId = labelsId;
    labelsId++;
    node->getChildren()[0]->accept(this);
    result += "  JUMPIF %while_start_" + std::to_string(curId) + "\n";
    result += "  JUMP %while_end_" + std::to_string(curId) + "\n";
    result += "%while_start_" + std::to_string(curId) + ":\n";
    node->getChildren()[1]->accept(this);
    node->getChildren()[0]->accept(this);
    result += "  JUMPIF %while_start_" + std::to_string(curId) + "\n";
    result += "%while_end_" + std::to_string(curId) + ":\n";
}

void CodeGenVisitor::visitIfStatement(TreeNode *node) {
    int curId = labelsId;
    labelsId++;
    node->getChildren()[0]->accept(this);
    result += "  JUMPIF %if_start_" + std::to_string(curId) + "\n";
    result += "  JUMP %else_start_" + std::to_string(curId) + "\n";
    result += "%if_start_" + std::to_string(curId) + ":\n";
    node->getChildren()[1]->accept(this);
    result += "  JUMP %if_else_end_" + std::to_string(curId) + "\n";
    result += "%else_start_" + std::to_string(curId) + ":\n";
    if (node->getChildren().size() > 2) {
        node->getChildren()[2]->accept(this);
    }
    result += "%if_else_end_" + std::to_string(curId) + ":\n";
}

void CodeGenVisitor::visitReturnStatement(TreeNode *node) {
    node->getChildren()[0]->accept(this);
    result += "  RETURN\n";
}

/**
 * result of every expression will ve at @result register
 *
 * two possibilities:
 * sequence of function calls
 * or not :))))
 */
void CodeGenVisitor::visitExpression(TreeNode *node) {
    if (node->getChildren().size() > 1) { // if we have more than 1 children that means there is a function call(s)
        node->getChildren()[0]->accept(this);
        result += "  PUT @caller, @result\n"; //store @result into @caller

        for (size_t i =1; i < node->getChildren().size(); ++i) {
            // i'th node is name of function call
            // we may have arguments or not
            if (i+1 < node->getChildren().size() && node->getChildren()[i+1]->getNodeType() == PE_ARGUMENTS) {// if we have arguments
                // pushing arguments into the registers
                node->getChildren()[i+1]->accept(this);
                result += "  CALL @caller, " + node->getChildren()[i]->getToken().str_value + "$" + idString + "\n";
                result += "  PUT @caller, @result\n";
                ++i; // skipping parameters
                result += "  PUT @result, @caller\n";
            } else { // if we don't have arguments
                result += "  CALL @caller, " + node->getChildren()[i]->getToken().str_value + "$\n";
            }
        }

    } else { // one children - one name of variable or literal
        node->getChildren()[0]->accept(this);
    }
}

void CodeGenVisitor::visitArguments(TreeNode *node) {
    auto typesMap = typeDeducingVisitor->getExpressionTypesMap();
    std::string temp;
    for (std::size_t i =0; i < node->getChildren().size(); i++) {
        auto currentNode = node->getChildren()[i];
        currentNode->accept(this);
        result += "  PUT @" + std::to_string(i) + ", @result\n";
        if (typesMap->find(currentNode) != typesMap->end()) {
            temp += std::to_string(typesMap->at(currentNode).first) + ".";
        } else {
            hasError = true;
            error = "accessing argument not in the types map, row: " + std::to_string(currentNode->getRowInSource()) +
                    " column: " + std::to_string(currentNode->getColumnInSource());
            throw 1;
        }
    }
    idString = temp.substr(0, temp.size()-1);
}

/**
 * just puts name in the node to result
 */
void CodeGenVisitor::visitName(TreeNode *node) {
    result += node->getToken().str_value;
}

bool CodeGenVisitor::errorStatus() const {
    return hasError;
}

const std::string &CodeGenVisitor::getErrorDescription() const {
    return error;
}

CodeGenVisitor::~CodeGenVisitor() {

}

const std::string &CodeGenVisitor::getResult() const {
    return result;
}

CodeGenVisitor::CodeGenVisitor(ClassTable *classTable, TypeDeducingVisitor *typeDeducingVisitor) : classTable(
        classTable), typeDeducingVisitor(typeDeducingVisitor) {}

