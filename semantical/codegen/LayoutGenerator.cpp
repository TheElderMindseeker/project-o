#include "LayoutGenerator.h"

#include <utility>

#include "../tables/ClassTable.h"
#include "../tables/ClassLayout.h"
#include "../tables/VariablesTable.h"
#include "../visitors/ClassStructureVisitor.h"
#include "../visitors/TypeDeducingVisitor.h"

LayoutGenerator::LayoutGenerator(const ClassTable *classTable,
                                 const ClassStructureVisitor *csVisitor,
                                 const TypeDeducingVisitor *tdVisitor) {
    auto typeMapping = tdVisitor->getExpressionTypesMap();

    // For each class present in ClassTable
    for (auto &entry : classTable->getClassPairs()) {
        // First, create ClassLayout without a parent ClassLayout
        // We will add parent later, during the second pass
        auto result = layouts.insert(std::make_pair(
            classTable->getClassId(entry.first),
            new ClassLayout(entry.first, nullptr)
        )).first;
        auto varTable = csVisitor->getStructures()
            .at(result->first)->variablesTable;

        // For each variable present in the chosen class varTable
        for (auto &var : varTable->getTable()) {
            // Add variable record as CLASSNAME$VARNAME to ClassLayout
            // The record will also hold var's type deduced by tdVisitor
            result->second->insertVariable(
                entry.first + "$" + var.first,
                typeMapping->at(const_cast<TreeNode *>(var.second)).second
            );
        }
    }

    // Second pass, during which we set up the inheritance relationships
    for (auto &entry : classTable->getClassPairs()) {
        auto classId = classTable->getClassId(entry.first);
        // The equality of class ID and its parent ID is the indicator of
        // class "Class" which is the beginning of all hierarchies
        if (classId != entry.second) {
            // Set parent class using class ID from classTable
            layouts[classId]->setParentClass(layouts.at(entry.second));
        }
    }
}

const std::map<uint32_t, ClassLayout *> &LayoutGenerator::getLayouts() const {
    return layouts;
}