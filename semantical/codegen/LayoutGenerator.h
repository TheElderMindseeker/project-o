#ifndef LAYOUT_GENERATOR_H
#define LAYOUT_GENERATOR_H

#include <map>

class ClassTable;
class ClassStructureVisitor;
class ClassLayout;
class TypeDeducingVisitor;

class LayoutGenerator {
public:
    LayoutGenerator(const ClassTable *classTable,
                    const ClassStructureVisitor *csVisitor,
                    const TypeDeducingVisitor *tdVisitor);

    const std::map<uint32_t, ClassLayout *> &getLayouts() const;

private:
    std::map<uint32_t, ClassLayout *> layouts;
};

#endif // LAYOUT_GENERATOR_H