#ifndef CONSTRUCTORS_TABLE_H
#define CONSTRUCTORS_TABLE_H

#include <iostream>
#include <vector>
#include <string>
#include <map>

class TreeNode;
class ClassTable;
class TemplateTable;

class ConstructorsTable {
    friend std::ostream &operator<<(std::ostream &os,
                                    const ConstructorsTable &t);

public:
    ConstructorsTable(const std::string &className,
                      const ClassTable *classTable);

    bool insert(const std::vector<std::string> &parameters,
                const TreeNode *body);

    bool has(const std::vector<std::string> &parameters) const;

    const TreeNode *get(const std::vector<std::string> &parameters) const;

private:
    const std::string convertParameters(
        const std::vector<std::string> &parameters
    ) const;

    std::string className;
    const ClassTable *classTable;

    std::map<std::string, const TreeNode *> table;
};

#endif // CONSTRUCTORS_TABLE_H