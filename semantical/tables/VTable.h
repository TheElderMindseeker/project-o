

#ifndef PROJECT_O_COMPILER_VTABLE_H
#define PROJECT_O_COMPILER_VTABLE_H


#include "ClassTable.h"
#include "MethodsTable.h"
#include "../visitors/ClassStructureVisitor.h"
#include <map>
#include <string>
#include <vector>
#include <utility>
#include <iostream>

class VTable {
public:
    VTable(ClassTable *classTable, const std::map<uint32_t, ClassStructure *> &classStructures);

    /**
     * structure of map is:
     * class name mapped to the vector
     * vector is an ordered list of pairs
     * where each pair is method signature and where this method is implemented
     * like: < "MethodName$Signature" , "@ClassName$MethodName$Signature" >
     * @return table of classes and methods inside them
     */
    const std::map<std::string, std::vector<std::pair<std::string, std::string>>> &getTable() const;

private:
    ClassTable * classTable;
    std::map<uint32_t, ClassStructure *> classStructures;

    std::map <std::string, std::vector<std::pair<std::string, std::string>>> table;

    void generateVTable();

    std::vector<std::pair<std::string, std::string>> processClass(std::string className);

    int getIdInTable(const std::string &basic_string, std::vector<std::pair<std::string, std::string>> vector);
};


#endif //PROJECT_O_COMPILER_VTABLE_H
