

#ifndef O_LANG_CLASSTABLE_H
#define O_LANG_CLASSTABLE_H

#include <vector>
#include <string>
#include <utility>
#include <unordered_map>

class GenericMagician;

class ClassTable {
public:

    void insertClass(const std::string &newClass, const std::string &parentClass="Class");

    /**
     * @param className
     * @return True if class is defined in the table, false otherwise
     */
    bool isClassDefined(const std::string &className) const;

    ClassTable(const GenericMagician *genericMagician);

    const std::vector<std::pair<std::string, uint32_t>> &getClassPairs() const;

    /**
     * @param className
     * @return id of class in classPairs structure
     * returns negative number if className is not present
     */
    int getClassId(const std::string &className) const;

    std::string getParentName(const std::string &className) const;

    uint32_t getParentID(const std::string &className);

private:
    std::vector<std::pair<std::string, uint32_t > > classPairs; // class name and class parent id
};


#endif //O_LANG_CLASSTABLE_H
