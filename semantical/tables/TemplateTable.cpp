
#include "TemplateTable.h"
#include <functional>

void
TemplateTable::insertClass(const std::string &newClass, const std::string &generic, const std::string &parentClass) {
    if (getClassId(newClass) < 0){
        templateTable.emplace_back(newClass, parentClass, generic);
    } else {
        throw std::invalid_argument( "class was defined" );
    }
}

int TemplateTable::isClassDefined(const std::string &className) {
    return getClassId(className) != -1;
}

TemplateTable::TemplateTable() {
    insertClass("Array", "T", "AnyRef");
    insertClass("List", "T", "AnyRef");
}

int TemplateTable::getClassId(const std::string &className) const {
    for (int i = 0; i < templateTable.size(); i++)
    {
        if (std::get<0>(templateTable[i]) == className)
        {
            return i;
        }
    }
    return -1;
}

const std::vector<std::tuple<std::string, std::string, std::string>> &TemplateTable::getTemplateTable() const {
    return templateTable;
}
