#ifndef METHODS_TABLE_H
#define METHODS_TABLE_H

#include <map>
#include <string>
#include <vector>
#include <utility>
#include <iostream>

class ClassTable;
class TemplateTable;
class TreeNode;

class MethodsTable {
    friend std::ostream &operator<<(std::ostream &os, const MethodsTable &t);

public:
    MethodsTable(const ClassTable *classTable);

    //! Insert new record into the methods table
    bool insert(const std::string &name,
                const std::vector<std::string> &parameters,
                const std::string &returnValue,
                const TreeNode *body);

    bool has(const std::string &name,
             const std::vector<std::string> &parameters) const;

    const TreeNode *get(const std::string &name,
                        const std::vector<std::string> &parameters) const;

    const std::string &getReturnType(
            const std::string &name,
            const std::vector<std::string> &parameters
    ) const;

    std::vector<std::string> getMethodSignatures() const;

private:
    const std::string convertParameters(
        const std::vector<std::string> &parameters
    ) const;

    const ClassTable *classTable;

    std::map<std::string, std::pair<std::string, const TreeNode *>> table;
};

#endif // METHODS_TABLE_H