

#include "VTable.h"


void VTable::generateVTable() {
    auto classPairs = classTable->getClassPairs();
    for (auto classPair : classPairs){
        table[classPair.first] = processClass(classPair.first);
    }
}

std::vector<std::pair<std::string, std::string>> VTable::processClass(std::string className) {
    auto classId = static_cast<uint32_t>(classTable->getClassId(className));

    std::string parentName = classTable->getParentName(className);
    std::vector<std::pair<std::string, std::string>> result;

    result = table[parentName];

    for (const auto &methodSignature : classStructures.at(classId)->methodsTable->getMethodSignatures()){

        int idOfCurrMethod = getIdInTable(methodSignature, result);

        auto currentMethod =
                std::pair<std::string, std::string>(methodSignature, "@"+className+"$"+methodSignature);

        if (idOfCurrMethod >= 0){
            result[idOfCurrMethod] = currentMethod;
        } else {
            result.push_back(currentMethod);
        }
    }

    return result;
}



VTable::VTable(ClassTable *classTable, const std::map<uint32_t, ClassStructure *> &classStructures) : classTable(
        classTable), classStructures(classStructures) {generateVTable();}


int VTable::getIdInTable(const std::string &methodSignature, std::vector<std::pair<std::string, std::string>> table) {
    for (int i = 0; i < table.size(); ++i) {
        if (table[i].first == methodSignature) {
            return i;
        }
    }
    return -1;
}

const std::map<std::string, std::vector<std::pair<std::string, std::string>>> &VTable::getTable() const {
    return table;
}

