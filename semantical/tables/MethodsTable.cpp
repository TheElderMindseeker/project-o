#include "MethodsTable.h"

#include "ClassTable.h"
#include "TemplateTable.h"
#include "../../syntactical/TreeNode.h"

MethodsTable::MethodsTable(const ClassTable *classTable)
    : classTable(classTable) {}

bool MethodsTable::insert(const std::string &name,
                          const std::vector<std::string> &parameters,
                          const std::string &returnValue,
                          const TreeNode *body) {
    std::string uniqueName = name + convertParameters(parameters);

    auto &&result = table.insert(
        std::make_pair(uniqueName, std::make_pair(returnValue, body))
    );
    return result.second;
}

bool MethodsTable::has(const std::string &name,
                       const std::vector<std::string> &parameters) const {
    std::string uniqueName = name + convertParameters(parameters);
    return table.count(uniqueName) > 0;
}

const TreeNode *MethodsTable::get(
    const std::string &name,
    const std::vector<std::string> &parameters
) const {
    if (has(name, parameters)) {
        std::string uniqueName = name + convertParameters(parameters);
        return table.at(uniqueName).second;
    }
    return nullptr;
}

const std::string &MethodsTable::getReturnType(
        const std::string &name,
        const std::vector<std::string> &parameters
) const {
    if (has(name, parameters)) {
        std::string uniqueName = name + convertParameters(parameters);
        return table.at(uniqueName).first;
    }
    throw 1;
}

std::vector<std::string> MethodsTable::getMethodSignatures() const {
    std::vector<std::string> result;
    for (auto &entry : table) {
        result.push_back(entry.first);
    }
    return result;
}

const std::string MethodsTable::convertParameters(
    const std::vector<std::string> &parameters
) const {
    std::string converted("$");
    const std::string dot(".");

    bool firstIteration = true;
    for (const auto &param : parameters) {
        if (firstIteration) {
            converted += std::to_string(classTable->getClassId(param));
            firstIteration = false;
        } else {
            converted += dot + std::to_string(classTable->getClassId(param));
        }
    }

    return converted;
}

std::ostream &operator<<(std::ostream &os, const MethodsTable &t) {
    for (auto &entry : t.table) {
        os << entry.first << "    "
           << " returning " << entry.second.first << '\n';
    }
    return os;
}