

#ifndef O_LANG_TEMPLATETABLE_H
#define O_LANG_TEMPLATETABLE_H

#include <vector>
#include <string>
#include <tuple>

class TemplateTable {
public:

    void insertClass(const std::string &newClass, const std::string &generic, const std::string &parentClass="Class");

    /**
     * @param className
     * @return positive number if class was defined, 0 otherwise
     */
    int isClassDefined(const std::string &className);

    TemplateTable();

    const std::vector<std::tuple<std::string, std::string, std::string>> &getTemplateTable() const;

    /**
     * @param className
     * @return id of class in templateTable structure
     * returns negative number if className is not present
     */
    int getClassId(const std::string &className) const;

private:

    /**
     * class name, parent name, generic name
     */
    std::vector<std::tuple<std::string, std::string, std::string>> templateTable;
    //std::vector<std::tuple<std::string, std::string, char>> templateTable;
};


#endif //O_LANG_TEMPLATETABLE_H
