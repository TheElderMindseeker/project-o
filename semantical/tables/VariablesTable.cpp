#include "VariablesTable.h"

#include <utility>

bool VariablesTable::insert(const std::string &name,
                            const TreeNode *expression) {
    auto &&result = table.insert(std::make_pair(name, expression));
    return result.second;
}

bool VariablesTable::has(const std::string &name) const {
    return table.count(name) > 0;
}

const TreeNode *VariablesTable::get(const std::string &name) const {
    if (has(name)) {
        return table.at(name);
    }
    return nullptr;
}

const std::map<std::string, const TreeNode *> &
        VariablesTable::getTable() const {
    return table;
}

std::ostream &operator<<(std::ostream &os, const VariablesTable &t) {
    for (auto &entry : t.table) {
        os << entry.first << ":\t" << entry.second->getTypeString() << '\n';
    }
    return os;
}