#include "ConstructorsTable.h"

#include <utility>

#include "../../syntactical/TreeNode.h"
#include "ClassTable.h"

ConstructorsTable::ConstructorsTable(
    const std::string &className,
    const ClassTable *classTable)
        : className(className),
          classTable(classTable) {}

bool ConstructorsTable::insert(const std::vector<std::string> &parameters,
                               const TreeNode *body) {
    std::string uniqueName(className + convertParameters(parameters));
    auto &&result = table.insert(std::make_pair(uniqueName, body));
    return result.second;
}

bool ConstructorsTable::has(const std::vector<std::string> &parameters) const {
    return table.count(convertParameters(parameters)) > 0;
}

const TreeNode *ConstructorsTable::get(
    const std::vector<std::string> &parameters
) const {
    if (has(parameters)) {
        std::string uniqueName(convertParameters(parameters));
        return table.at(uniqueName);
    }
    return nullptr;
}

const std::string ConstructorsTable::convertParameters(
    const std::vector<std::string> &parameters
) const {
    std::string converted("$");
    const std::string dot(".");

    bool firstIteration = true;
    for (const auto &param : parameters) {
        if (firstIteration) {
            converted += std::to_string(classTable->getClassId(param));
            firstIteration = false;
        } else {
            converted += dot + std::to_string(classTable->getClassId(param));
        }
    }

    return converted;
}

std::ostream &operator<<(std::ostream &os, const ConstructorsTable &t) {
    for (auto &entry : t.table) {
        os << entry.first << '\n';
    }
    return os;
}