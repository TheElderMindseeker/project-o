#ifndef VARIABLES_TABLE_H
#define VARIABLES_TABLE_H

#include <iostream>
#include <string>
#include <map>

#include "../../syntactical/TreeNode.h"

class VariablesTable {
    friend std::ostream &operator<<(std::ostream &os, const VariablesTable &t);

public:
    //! Insert new record into the table
    /*!
      Inserts new element into the table.  If the element with the specified
      key was already there, the function will return false, which means that
      insertion was unsuccessful.  Otherwise it will return true meaning
      the new record just appeared in the table.
    */
    bool insert(const std::string &name, const TreeNode *expression);

    bool has(const std::string &name) const;

    //! Get the TreeNode element by variable name
    /*!
      Get the TreeNode element by variable name. Returns nullptr if the fetch
      is unsuccessful.
    */
    const TreeNode *get(const std::string &name) const;

    const std::map<std::string, const TreeNode *> &getTable() const;

private:
    std::map<std::string, const TreeNode *> table;
};

#endif // VARIABLES_TABLE_H