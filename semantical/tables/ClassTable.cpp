#include "ClassTable.h"
#include <iostream>

#include "../generics/GenericMagician.h"

ClassTable::ClassTable(const GenericMagician *genericMagician) {
    classPairs.push_back(*new std::pair<std::string, std::uint32_t >("Class", 0));
    insertClass("AnyValue");

    insertClass("Integer", "AnyValue");
    insertClass("Real", "AnyValue");
    insertClass("Boolean", "AnyValue");

    insertClass("AnyRef");

    for (auto &actualParam : genericMagician->getArraySetups()) {
        insertClass("Array$" + actualParam, "AnyRef");
    }

    for (auto &actualParam : genericMagician->getListSetups()) {
        insertClass("List$" + actualParam, "AnyRef");
    }
}

/**
 *
 * @param newClass
 * @param parentClass default value is "Class"
 */
void ClassTable::insertClass(const std::string &newClass, const std::string &parentClass) {
    int parentId = getClassId(parentClass);
    if (getClassId(newClass) < 0 // checking if new class wasn't defined
    && parentId >= 0 // checking if parent is present
    ) {
        uint32_t u_parentId = static_cast<const uint32_t &>(parentId);
        auto currentPair = new std::pair<std::string, std::uint32_t >(newClass, u_parentId);
        classPairs.push_back(*currentPair);
    } else {
        throw std::invalid_argument( "class was defined or parent doesn't exists" );
    }
}

std::string ClassTable::getParentName(const std::string &className) const {
    uint32_t  parentId = classPairs[getClassId(className)].second;
    return classPairs[parentId].first;
}


uint32_t ClassTable::getParentID(const std::string &className) {
    return classPairs[getClassId(className)].second;
}

int ClassTable::getClassId(const std::string &className) const {
    for (int i = 0; i < classPairs.size(); i++)
    {
        if (classPairs[i].first == className)
        {
            return i;
        }
    }
    return -1;
}

bool ClassTable::isClassDefined(const std::string &className) const {
    return getClassId(className) != -1;
}

const std::vector<std::pair<std::string, uint32_t>> &ClassTable::getClassPairs() const {
    return classPairs;
}
