#ifndef CLASS_LAYOUT_H
#define CLASS_LAYOUT_H

#include <map>
#include <vector>
#include <string>

class ClassLayout {
public:
    ClassLayout(const std::string &className, ClassLayout *parentClass);

    bool insertVariable(const std::string &name, const std::string &type);

    void setParentClass(ClassLayout *parentClass);

    std::string getLayout() const;

private:
    void populateLayout(std::vector<std::string> &layoutEntries) const;

    const std::string className;
    ClassLayout *parentClass;
    std::map<std::string, std::string> layout;
};

#endif // CLASS_LAYOUT_H