#include "ClassLayout.h"

#include <utility>
#include <sstream>

ClassLayout::ClassLayout(const std::string &className,
                         ClassLayout *parentClass)
    : parentClass(parentClass), className(className) {}

bool ClassLayout::insertVariable(const std::string &name,
                                 const std::string &type) {
    auto &&result = layout.insert(std::make_pair(name, type));
    return result.second;
}

void ClassLayout::setParentClass(ClassLayout *parentClass) {
    ClassLayout::parentClass = parentClass;
}

std::string ClassLayout::getLayout() const {
    std::vector<std::string> layoutEntries;
    populateLayout(layoutEntries);

    std::stringstream output;
    output << "layout " << className << " \n";
    for (auto &var : layoutEntries) {
        output << "  " << var << "\n";
    }

    return output.str();
}

void ClassLayout::populateLayout(
    std::vector<std::string> &layoutEntries
) const {
    if (parentClass != nullptr) {
        parentClass->populateLayout(layoutEntries);
    }

    std::string colon(": ");
    for (auto &entry : layout) {
        layoutEntries.push_back(entry.first + colon + entry.second);
    }
}