#include <iostream>
#include "SemanticalAnalyzer.h"
#include "visitors/ImagingVisitor.h"
#include "helpers/TablesPrinter.h"
#include "generics/GenericMagician.h"

void SemanticalAnalyzer::parse(TreeNode * root) {
    ImagingVisitor * imVisitor = new ImagingVisitor(classTable);
    root->accept(imVisitor);
    if (imVisitor->errorStatus()) {
        hasError = true;
        errorDescription = imVisitor->getErrorDescription();
    }
}

SemanticalAnalyzer::SemanticalAnalyzer(
    const GenericMagician *genericMagician
) {
    classTable = new ClassTable(genericMagician);
}

ClassTable *SemanticalAnalyzer::getClassTable() const {
    return classTable;
}

bool SemanticalAnalyzer::isHasError() const {
    return hasError;
}

const std::string &SemanticalAnalyzer::getErrorDescription() const {
    return errorDescription;
}
