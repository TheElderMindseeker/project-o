#include "GenericMagician.h"

#include <iostream>

#include "../../syntactical/TreeNode.h"
#include "../visitors/ClassSorterVisitor.h"
#include "../visitors/GenericCrawlerVisitor.h"
#include "../visitors/GenericSetuperVisitor.h"

GenericMagician::GenericMagician(TreeNode *programNode) {
    auto csVisitor = new ClassSorterVisitor();
    programNode->accept(csVisitor);

    if (csVisitor->errorStatus()) {
        error = true;
        errorDescription = csVisitor->getErrorDescription();
        return;
    }

    std::vector<std::string> classes;
    for (auto &entry : csVisitor->getClasses()) {
        classes.push_back(entry.first);
    }

    std::vector<std::string> templates;
    for (auto &entry : csVisitor->getTemplates()) {
        templates.push_back(entry.first);
    }

    auto gcVisitor = new GenericCrawlerVisitor(classes, templates);
    programNode->accept(gcVisitor);

    if (gcVisitor->errorStatus()) {
        error = true;
        errorDescription = gcVisitor->getErrorDescription();
        return;
    }

    arraySetups = gcVisitor->getArraySetups();
    listSetups = gcVisitor->getListSetups();

    for (auto &actualParam : listSetups) {
        std::cerr << "\n\n+++++++++++++++++++++++++++++++++++++++++++++\n";
        std::cerr << actualParam;
        std::cerr << "\n+++++++++++++++++++++++++++++++++++++++++++++\n\n";
    }

    newTree = new TreeNode(programNode->getNodeType(),
                           programNode->getRowInSource(),
                           programNode->getColumnInSource());

    auto gsVisitor = new GenericSetuperVisitor();

    if (gsVisitor->errorStatus()) {
        error = true;
        errorDescription = gsVisitor->getErrorDescription();
        return;
    }

    auto userSetups = gcVisitor->getUserSetups();

    for (TreeNode *child : programNode->getChildren()) {
        auto &&className = getClassName(child);

        // This means the class is generic and needs to be setup
        // for one or several actual parameters
        if (userSetups.count(className) > 0) {
            for (auto &actualName : userSetups.at(className)) {
                gsVisitor->setActualParameter(actualName);
                child->accept(gsVisitor);
                auto newClassDecl = gsVisitor->getSetupClass();
                newTree->appendChild(newClassDecl);
            }
        } else {
            child->accept(gsVisitor);
            auto newClassDecl = gsVisitor->getSetupClass();
            newTree->appendChild(newClassDecl);
        }

        child->accept(gsVisitor);
    }

    //std::cerr << newTree->toJSON();
}

TreeNode *GenericMagician::getNewTree() {
    return newTree;
}

const std::set<std::string> &GenericMagician::getArraySetups() const {
    return arraySetups;
}

const std::set<std::string> &GenericMagician::getListSetups() const {
    return listSetups;
}

bool GenericMagician::errorStatus() const {
    return error;
}

const std::string &GenericMagician::getErrorDescription() const {
    return errorDescription;
}

const std::string &GenericMagician::getClassName(
    TreeNode *classDeclNode
) const {
    auto classNameNode = classDeclNode->getChildren().at(0);
    auto nameNode = classNameNode->getChildren().at(0);
    return nameNode->getToken().str_value;
}