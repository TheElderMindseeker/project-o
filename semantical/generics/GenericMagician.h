#ifndef GENERIC_MAGICIAN_H
#define GENERIC_MAGICIAN_H

#include <set>
#include <string>

class TreeNode;

class GenericMagician {
public:
    GenericMagician(TreeNode *programNode);

    TreeNode *getNewTree();

    const std::set<std::string> &getArraySetups() const;
    const std::set<std::string> &getListSetups() const;

    bool errorStatus() const;
    const std::string &getErrorDescription() const;

private:
    const std::string &getClassName(TreeNode *classDeclNode) const;

    bool error = false;
    std::string errorDescription;

    std::set<std::string> arraySetups;
    std::set<std::string> listSetups;

    TreeNode *newTree;
};

#endif // GENERIC_MAGICIAN_H