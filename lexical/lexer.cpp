#include "lexer.h"
#include <stdio.h>

const char *TokenException::what() const noexcept {
    std::string* str = new std::string("[" + std::to_string(row)
            + ":" + std::to_string(col) + "] Unrecodnized token");
    return str->c_str();
}

TokenException::TokenException(int row, int col) : row(row), col(col) {}

Lexer::Lexer(std::string file) {
    pos = 0;
    token_pos = 0;
    token_row = 1u;
    token_column = 1u;
    tokenize(file);
    lex_tokens = tokens;
    size = lex_tokens.size();
}

unsigned long Lexer::get_size() {
    return size;
}

Token Lexer::get_next_token() {
    if (token_pos < lex_tokens.size()) {
        Token tmp = lex_tokens.at(token_pos);
        print_token(tmp);
        token_pos++;
        return tmp;
    } else {
        Token tmp;
        tmp.type = TK_ENDMARKER;
        return tmp;
    }
}

std::vector<Token> Lexer::get_tokens() {
    return lex_tokens;
}

void Lexer::print_token(Token token) {
    std::cout << token.type << std::endl;
    std::cout << token.str_value << std::endl;
    std::cout << token.int_value << std::endl;
    std::cout << token.real_value << std::endl;
    std::cout << token.row << std::endl;
    std::cout << token.column << std::endl;
    std::cout << "------------------------" << std::endl;
}

void Lexer::tokenize(std::string file) {
    while (!is_end(file, pos)) {
        std::string buf = "";
        Token tmp;
        while (!is_end(file, pos)
               && (isblank(file.at(pos)) || std::iscntrl(file.at(pos)))) {
            if (isblank(file.at(pos))) {
                if (file.at(pos) == '\t'){
                    token_column += 4;
                } else {
                    token_column++;
                }
            } else if (file.at(pos) == '\n') {
                token_row++;
                token_column = 1;
            }
            pos++;
        }

        if (!is_end(file, pos)
            && (std::isalpha(file.at(pos)) || file.at(pos) == '_')) {
            buf.append(1, file.at(pos));
            tmp.row = token_row;
            tmp.column = token_column;
            pos++;
            while (!is_end(file, pos)
                   && (std::isalnum(file.at(pos)) || file.at(pos) == '_')) {
                buf.append(1, file.at(pos));
                pos++;
            }
            token_column += buf.length();
            if (buf == "class") {
                tmp.type = TokenType::TK_CLASS;
            } else if (buf == "else") {
                tmp.type = TokenType::TK_ELSE;
            } else if (buf == "end") {
                tmp.type = TokenType::TK_END;
            } else if (buf == "extends") {
                tmp.type = TokenType::TK_EXTENDS;
            } else if (buf == "if") {
                tmp.type = TokenType::TK_IF;
            } else if (buf == "is") {
                tmp.type = TokenType::TK_IS;
            } else if (buf == "loop") {
                tmp.type = TokenType::TK_LOOP;
            } else if (buf == "method") {
                tmp.type = TokenType::TK_METHOD;
            } else if (buf == "return") {
                tmp.type = TokenType::TK_RETURN;
            } else if (buf == "then") {
                tmp.type = TokenType::TK_THEN;
            } else if (buf == "this") {
                tmp.type = TokenType::TK_THIS;
            } else if (buf == "var") {
                tmp.type = TokenType::TK_VAR;
            } else if (buf == "while") {
                tmp.type = TokenType::TK_WHILE;
            } else if (buf == "True") {
                tmp.type = TokenType::TK_BOOLEAN;
                tmp.int_value = 1;
            } else if (buf == "False") {
                tmp.type = TokenType::TK_BOOLEAN;
                tmp.int_value = 0;
            } else {
                tmp.type = TokenType::TK_NAME;
                tmp.str_value = buf;
            }
            tokens.push_back(tmp);
        } else if (!is_end(file, pos) && file.at(pos) == '(') {
            tmp.row = token_row;
            tmp.column = token_column;
            buf.append(1, file.at(pos));
            pos++;
            token_column += buf.length();
            tmp.type = TokenType::TK_LEFT_PAREN;
            tokens.push_back(tmp);
        } else if (!is_end(file, pos) && file.at(pos) == ')') {
            tmp.row = token_row;
            tmp.column = token_column;
            buf.append(1, file.at(pos));
            pos++;
            token_column += buf.length();
            tmp.type = TokenType::TK_RIGHT_PAREN;
            tokens.push_back(tmp);
        } else if (!is_end(file, pos) && file.at(pos) == '[') {
            tmp.row = token_row;
            tmp.column = token_column;
            buf.append(1, file.at(pos));
            pos++;
            token_column += buf.length();
            tmp.type = TokenType::TK_LEFT_BRACK;
            tokens.push_back(tmp);
        } else if (!is_end(file, pos) && file.at(pos) == ']') {
            tmp.row = token_row;
            tmp.column = token_column;
            buf.append(1, file.at(pos));
            pos++;
            token_column += buf.length();
            tmp.type = TokenType::TK_RIGHT_BRACK;
            tokens.push_back(tmp);
        } else if (!is_end(file, pos) && file.at(pos) == ',') {
            tmp.row = token_row;
            tmp.column = token_column;
            buf.append(1, file.at(pos));
            pos++;
            token_column += buf.length();
            tmp.type = TokenType::TK_COMMA;
            tokens.push_back(tmp);
        } else if (!is_end(file, pos) && file.at(pos) == ':') {
            tmp.row = token_row;
            tmp.column = token_column;
            buf.append(1, file.at(pos));
            pos++;
            tmp.type = TokenType::TK_COLON;
            if (!is_end(file, pos) && file.at(pos) == '=') {
                buf.append(1, file.at(pos));
                pos++;
                tmp.type = TokenType::TK_ASSIGNMENT;
            }
            token_column += buf.length();
            tokens.push_back(tmp);
        } else if (!is_end(file, pos) && file.at(pos) == '.') {
            tmp.row = token_row;
            tmp.column = token_column;
            buf.append(1, file.at(pos));
            pos++;
            tmp.type = TokenType::TK_ACCESS;
            if (!is_end(file, pos) && std::isdigit(file.at(pos))) {
                buf.append(1, file.at(pos));
                pos++;
                while (!is_end(file, pos) && std::isdigit(file.at(pos))) {
                    buf.append(1, file.at(pos));
                    pos++;
                }
                tmp.type = TokenType::TK_REAL;
                tmp.real_value = std::stod(buf);
            }
            token_column += buf.length();
            tokens.push_back(tmp);
        } else if (!is_end(file, pos) && std::isdigit(file.at(pos))) {
            tmp.row = token_row;
            tmp.column = token_column;
            buf.append(1, file.at(pos));
            pos++;
            while (!is_end(file, pos) && std::isdigit(file.at(pos))) {
                buf.append(1, file.at(pos));
                pos++;
            }
            tmp.type = TokenType::TK_INTEGER;
            tmp.int_value = std::stoi(buf);

            if (!is_end(file, pos) && file.at(pos) == '.') {
                buf.append(1, file.at(pos));
                pos++;
                while (!is_end(file, pos) && std::isdigit(file.at(pos))) {
                    buf.append(1, file.at(pos));
                    pos++;
                }
                tmp.type = TokenType::TK_REAL;
                tmp.real_value = std::stod(buf);
            }
            token_column += buf.length();
            tokens.push_back(tmp);
        } else {
            if (is_end(file, pos)) {
                break;
            }
            std::cerr << buf << std::endl;
            throw TokenException(token_row, token_column);
        }
    }
}

bool Lexer::is_end(std::string file, int pos) {
    return pos >= file.size();
}