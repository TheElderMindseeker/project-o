#ifndef PROJECT_LEXER_H
#define PROJECT_LEXER_H

#include <iostream>
#include <cctype>
#include <fstream>
#include <vector>
#include <cstring>
#include <exception>

struct TokenException : public std::exception {
    TokenException(int row, int col);

    const char *what() const noexcept;
    int row, col;
};

enum TokenType {
    TK_CLASS,
    TK_ELSE,
    TK_END,
    TK_EXTENDS,
    TK_IF,
    TK_IS,
    TK_LOOP,
    TK_METHOD,
    TK_RETURN,
    TK_THEN,
    TK_THIS,
    TK_VAR,
    TK_WHILE,
    TK_LEFT_PAREN,
    TK_RIGHT_PAREN,
    TK_LEFT_BRACK,
    TK_RIGHT_BRACK,
    TK_COLON,
    TK_COMMA,
    TK_ASSIGNMENT,
    TK_ACCESS,
    TK_NAME,
    TK_INTEGER,
    TK_REAL,
    TK_BOOLEAN,
    TK_ENDMARKER
};

struct Token {
    TokenType type;
    std::string str_value;
    long long int int_value;
    double real_value;
    unsigned int row;
    unsigned int column;
};

class Lexer {
public:
    Lexer(std::string file);

    unsigned long get_size();

    Token get_next_token();

    std::vector<Token> get_tokens();

    void print_token(Token token);

    void tokenize(std::string file);

    bool is_end(std::string file, int pos);

private:
    int pos;
    int token_pos;
    uint32_t token_row;
    uint32_t token_column;
    unsigned long size;
    std::vector<Token> lex_tokens;
    std::vector<Token> tokens = {};
};

#endif //PROJECT_LEXER_H